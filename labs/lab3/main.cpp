#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
using std::vector; 
using std::cout; 
using std::cin; 
using std::endl;


int animationType = 0; //used to tell which animation to display
bool press = false;    //to check for space button press
int maxAnimations = 4;
const int matSize = 4; //max size of matrix


//creates an identity matrix
vector<vector<float>> matrix(int rows, int cols) {
	vector<vector<float>> m;

	for (int i = 0; i < rows; i++) {
		//create new row
		vector<float> v;
		m.push_back(v);
		for (int j = 0; j < cols; j++) {
			//1's along diagonal
			if (i == j) {
				m[i].push_back(1.0f);
			}
			else {
				m[i].push_back(0.0f);
			}
		}
	}

	//return vector<vector<float>>(rows, vector<float>(cols, 0.0f));
	return m;

}

vector<vector<float>> operator*(const vector<vector<float>>& m1, const vector<vector<float>>& m2) {
	vector<vector<float>> m3 = matrix(m1.size(), m2[0].size());

	//if matrices are wrong size for multiplication
	if (m1[0].size() != m2.size()) {
		cout << "matrix multiplication fail" << endl;
		return m3;
	}

	for (int i = 0; i < m1.size(); i++) {
		for (int j = 0; j < m2[0].size(); j++) {
			float sum = 0;
			for (int k = 0; k < m2.size(); k++) {
				sum += m1[i][k] * m2[k][j];
			}
			m3[i][j] = sum;
		}

	}
	return m3;
}

vector<vector<float>> operator-(const vector<vector<float>>& m1, const vector<vector<float>>& m2) {
	vector<vector<float>> m3 = matrix(m1.size(), m1.size());

	if (m1.size() != m2.size() || m1[0].size() != m2[0].size()) {
		cout << "matrix subtraction fail" << endl;

		return m3;
	}

	for (int i = 0; i < m1.size(); i++) {
		for (int j = 0; j < m1[i].size(); j++) {
			m3[i][j] = m1[i][j] - m2[i][j];
		}

	}
	return m3;
}

vector<vector<float>> operator+(const vector<vector<float>>& m1, const vector<vector<float>>& m2) {
	vector<vector<float>> m3 = matrix(m1.size(), m1.size());

	if (m1.size() != m2.size() || m1[0].size() != m2[0].size()) {
		cout << "matrix addition fail" << endl;

		return m3;
	}

	for (int i = 0; i < m1.size(); i++) {
		for (int j = 0; j < m1[i].size(); j++) {
			m3[i][j] = m1[i][j] + m2[i][j];
		}

	}
	return m3;
}

//allows for printing of a matrix
std::ostream& operator<< (std::ostream &os, const vector<vector<float>> m)
{
	for (int i = 0; i < m.size(); i++) {
		for (int j = 0; j < m[i].size(); j++) {
			os << " " << m[i][j] << " ";
		}
		os << endl;
	}
	return os;
}
void printM(vector<vector<float>> m) {
	cout << m << endl;
}


//scale matrix by scaling vector amount
vector<vector<float>> scaleM(vector<vector<float>> m, vector<float> scale) {
	if (scale.size() != m.size() - 1) {
		cout << "matrix scaling fail" << endl;
		return m;
	}

	//create scaling matrix
	vector<vector<float>> s = matrix(m.size(), m.size());
	//along the diagonal except for bottom right, input scaling factors
	for (int i = 0; i < s.size()-1; i++) { 
		s[i][i] = scale[i];
	}

	//apply scaling to matrix by matrix multiplication
	m = s * m;
	return m;
}

//rotate matrix by degree amount
vector<vector<float>> rotateM(vector<vector<float>> m, float degree) {

	//create rotation matrix
	vector<vector<float>> r = matrix(m.size(), m.size());

	for (int i = 0; i < r.size(); i++) {
		if (i == r.size()-1) { //skip last row
			continue;
		}
		else {

		}
		for (int j = 0; j < r[i].size(); j++) {
			if (j == r[i].size() - 1) { //skip last column
				continue;
			}
			if (i == j) {
				r[i][j] = cos(degree);
			}
			else if (i == 0 && j == 1) {
				r[i][j] = -sin(degree);
			}
			else if (i == 1 && j == 0) {
				r[i][j] = sin(degree);
			}
		}
	}
	//apply rotation to matrix by matrix multiplication
	m = r * m;

	return m;
}

vector<vector<float>> translateM(vector<vector<float>> m, vector<float> translation) {
	// translation: (x,y,z)
	//only translating in X and Y 
	if (translation.size() != m.size() - 1) {
		cout << "matrix translating fail" << endl;
		return m;
	}

	//create translation matrix
	vector<vector<float>> t = matrix(m.size(), m.size());
	for (int i = 0; i < translation.size(); i++) {
		t[i][t[0].size() - 1] = translation[i];
	}
	
	//apply translation by multiplication
	m = t * m;
	return m;
}

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
		press = true;
	}
	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_RELEASE) {
		if (press == true) {
			animationType++;
			if (animationType > maxAnimations) {
				animationType = 0;
			}
			press = false;
		}
	}

}


int main(void) {
	

    /* Initialize the library */
    GLFWwindow* window;
    if (!glfwInit()) {
        return -1;
    }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Lab 3", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the triangle drawing */
    // define the vertex coordinates of the triangle
    float triangle[] = {
         0.5f,  0.5f, 1.0, 0.0, 0.0,
         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,

		0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f, -0.5f, 0.0, 0.0, 1.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,
    };

    // create and bind the vertex buffer object and copy the data to the buffer
    GLuint VBO[1];
    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);

    // create and bind the vertex array object and describe data layout
    GLuint VAO[1];
    glGenVertexArrays(1, VAO);
    glBindVertexArray(VAO[0]);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(0*sizeof(float)));
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 5*sizeof(float), (void*)(2*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // use the shader
        shader.use();
		vector<vector<float>> transf = matrix(4, 4);

        /** Part 2 animate and scene by updating the transformation matrix */
		if (animationType == 0) { //static mode
			//pass

		} else if (animationType == 1) { //rotate center mode
			float timeValue = (float)glfwGetTime();

			transf = rotateM(transf, timeValue);

			

		} else if (animationType == 2) { //rotate off mode
			float timeValue = (float)glfwGetTime();

				//rotate around a point
				vector<float> v2 = { 0.5f, 0.5f, 0.0f };
				transf = translateM(transf, v2);
				transf = rotateM(transf, timeValue);

				vector<float> v3 = { -0.5f, -0.5f, 0.0f };
				transf = translateM(transf, v3);

		} else if (animationType == 3) { //scale mode

			float timeValue = (float)glfwGetTime();
			float scaleAmount = abs(sin(timeValue));
			vector<float> v = { scaleAmount, scaleAmount, 1.0f};

			transf = scaleM(transf, v);

		} else if (animationType == 4) { //impress me mode
			float timeValue = (float)glfwGetTime();
			float scaleAmount = abs(sin(timeValue));
			vector<float> v = { scaleAmount, scaleAmount, 1.0f };

			float transAmount = abs(cos(timeValue)*sin(timeValue));
			vector<float> v2 = { transAmount, transAmount, 0.0f };

			transf = scaleM(transf, v);
			transf = translateM(transf, v2);
			transf = rotateM(transf, timeValue);
			
		} 
		// draw our triangles
		glBindVertexArray(VAO[0]);
		glDrawArrays(GL_TRIANGLES, 0, sizeof(triangle));


		//convert matrix using vectors, to matrix with contiguous data
		//needed to be done in order to be passed to vertex shader properly
		float converted[matSize][matSize];
		for (int i = 0; i < transf.size(); i++) {
			for (int j = 0; j < transf[i].size(); j++) {
				converted[i][j] = transf[i][j];
			}
		}

		//update uniform transform matrix
		unsigned int transformLoc = glGetUniformLocation(shader.id(), "transf");
		glUniformMatrix4fv(transformLoc, 1, GL_TRUE, &converted[0][0]);

		

        /* Swap front and back * buffers */
        glfwSwapBuffers(window);

        /* Poll for and * process * events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
