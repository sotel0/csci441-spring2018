#version 330 core
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec3 aColor;

out vec3 myColor;
uniform	mat4 transf;

void main() {
    gl_Position = transf * vec4(aPos, 0.0, 1.0);
	
    myColor = aColor;
}
