#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <vector>

#define PI 3.1415927

using std::vector;
using std::cout;
using std::cin;
using std::endl;

//const int SCREEN_WIDTH = 640;
const int SCREEN_WIDTH = 640+200;
const int SCREEN_HEIGHT = 480+200;
//const int SCREEN_HEIGHT = 480;

const int matSize = 4; //max size of matrix
int inputMode = 0;
enum projection { orthogonal,perspective };
projection proj = orthogonal;
enum primitive { cube, cylinder };
primitive shape = cube;
bool spacePress = false;
bool slashPress = false;
bool once = true;


//creates an identity matrix
vector<vector<float>> matrix(int rows, int cols) {
	vector<vector<float>> m;

	for (int i = 0; i < rows; i++) {
		//create new row
		vector<float> v;
		m.push_back(v);
		for (int j = 0; j < cols; j++) {
			//1's along diagonal
			if (i == j) {
				m[i].push_back(1.0f);
			}
			else {
				m[i].push_back(0.0f);
			}
		}
	}

	//return vector<vector<float>>(rows, vector<float>(cols, 0.0f));
	return m;

}

vector<vector<float>> operator*(const vector<vector<float>>& m1, const vector<vector<float>>& m2) {
	vector<vector<float>> m3 = matrix(m1.size(), m2[0].size());

	//if matrices are wrong size for multiplication
	if (m1[0].size() != m2.size()) {
		cout << "matrix multiplication fail" << endl;
		return m3;
	}

	for (int i = 0; i < m1.size(); i++) {
		for (int j = 0; j < m2[0].size(); j++) {
			float sum = 0;
			for (int k = 0; k < m2.size(); k++) {
				sum += m1[i][k] * m2[k][j];
			}
			m3[i][j] = sum;
		}

	}
	return m3;
}

vector<vector<float>> operator-(const vector<vector<float>>& m1, const vector<vector<float>>& m2) {
	vector<vector<float>> m3 = matrix(m1.size(), m1.size());

	if (m1.size() != m2.size() || m1[0].size() != m2[0].size()) {
		cout << "matrix subtraction fail" << endl;

		return m3;
	}

	for (int i = 0; i < m1.size(); i++) {
		for (int j = 0; j < m1[i].size(); j++) {
			m3[i][j] = m1[i][j] - m2[i][j];
		}

	}
	return m3;
}

vector<vector<float>> operator+(const vector<vector<float>>& m1, const vector<vector<float>>& m2) {
	vector<vector<float>> m3 = matrix(m1.size(), m1.size());

	if (m1.size() != m2.size() || m1[0].size() != m2[0].size()) {
		cout << "matrix addition fail" << endl;

		return m3;
	}

	for (int i = 0; i < m1.size(); i++) {
		for (int j = 0; j < m1[i].size(); j++) {
			m3[i][j] = m1[i][j] + m2[i][j];
		}

	}
	return m3;
}

vector<float> operator-(const vector<float>& m1, const vector<float>& m2) {
	vector<float> m3 = {0,0,0};

	if (m1.size() != m2.size()) {
		cout << "matrix addition fail" << endl;
		return m3;
	}

	for (int i = 0; i < m1.size(); i++) {
			m3[i] = m1[i] - m2[i];
	}
	return m3;
}

//allows for printing of a matrix
std::ostream& operator<< (std::ostream &os, const vector<vector<float>> m)
{
	for (int i = 0; i < m.size(); i++) {
		for (int j = 0; j < m[i].size(); j++) {
			os << " " << m[i][j] << " ";
		}
		os << endl;
	}
	return os;
}

std::ostream& operator<< (std::ostream &os, const vector<float> v)
{
	for (int i = 0; i < v.size(); i++) {
		os << " " << v[i] << " ";
	}
	os << endl;
	return os;
}

void printM(vector<vector<float>> m) {
	cout << m << endl;
}
void printV(vector<float> v) {
	cout << v << endl;
}


//scale matrix by scaling vector amount
vector<vector<float>> scaleM(vector<vector<float>> m, vector<float> scale) {
	if (scale.size() != m.size() - 1) {
		cout << "matrix scaling fail" << endl;
		return m;
	}

	//create scaling matrix
	vector<vector<float>> s = matrix(m.size(), m.size());
	//along the diagonal except for bottom right, input scaling factors
	for (int i = 0; i < s.size() - 1; i++) {
		s[i][i] = scale[i];
	}

	//apply scaling to matrix by matrix multiplication
	m = s * m;
	return m;
}

//rotate matrix by degree amount
vector<vector<float>> rotateM(vector<vector<float>> m, float degree, char axis) {

	/*vector<float> origin = { -m[0][3],-m[1][3],-m[2][3] };
	vector<float> position = { m[0][3],m[1][3],m[2][3] };*/

	//create rotation matrix
	vector<vector<float>> r = matrix(m.size(), m.size());
	
	//rotate along z axis
	if (axis == 'z' || axis == 'Z') {
		for (int i = 0; i < r.size(); i++) {
			if (i == r.size() - 1) { //skip last row
				continue;
			}
			for (int j = 0; j < r[i].size(); j++) {
				if (j == r[i].size() - 1) { //skip last column
					continue;
				}
				if (i == j) {
					r[i][j] = cos(degree);
				}
				else if (i == 0 && j == 1) {
					r[i][j] = -sin(degree);
				}
				else if (i == 1 && j == 0) {
					r[i][j] = sin(degree);
				}
			}
		}
	}

	//rotate along y axis
	if (axis == 'y' || axis == 'Y') {
		for (int i = 0; i < r.size(); i++) {
			if (i == 1) { //skip second row
				continue;
			}
			for (int j = 0; j < r[i].size(); j++) {
				if (j == 1) { //skip second column
					continue;
				}
				if ((i == 0 && j ==0) ||(i ==2 && j ==2)) {
					r[i][j] = cos(degree);
				}
				else if (i == 0 && j == 2) {
					r[i][j] = sin(degree);
				}
				else if (i == 2 && j == 0) {
					r[i][j] = -sin(degree);
				}
			}
		}
	}

	//rotate along x axis
	if (axis == 'x' || axis == 'X') {
		for (int i = 0; i < r.size(); i++) {
			if (i == 0) { //skip first row
				continue;
			}
			for (int j = 0; j < r[i].size(); j++) {
				if (j == 0) { //skip first column
					continue;
				}
				if ((i == 1 && j == 1) || (i == 2 && j == 2)) {
					r[i][j] = cos(degree);
				}
				else if (i == 1 && j == 2) {
					r[i][j] = -sin(degree);
				}
				else if (i == 2 && j == 1) {
					r[i][j] = sin(degree);
				}
			}
		}
	}


	//apply rotation to matrix by matrix multiplication
	m = r * m;

	return m;
}

//translate matrix by vector direction
vector<vector<float>> translateM(vector<vector<float>> m, vector<float> translation) {
	// translation: (x,y,z)
	//only translating in X and Y 
	if (translation.size() != m.size() - 1) {
		cout << "matrix translating fail" << endl;
		return m;
	}

	//create translation matrix
	vector<vector<float>> t = matrix(m.size(), m.size());

	for (int i = 0; i < translation.size(); i++) {
		t[i][3] = translation[i];
	}

	//apply translation by multiplication
	m = t * m;
	return m;
}

vector<float> crossV(vector<float> a, vector<float> b) {
	if (b.size() != 3) {
		cout << "cross product fail" << endl;
		return a;
	}

	vector<float> cross = 
	{a[1] * b[2] - a[2] * b[1],
	a[2] * b[0] - a[0] * b[2],
	a[0] * b[1] - a[1] * b[0] };
	return cross;
}

float magV(vector<float> v) {
	float mag = pow(pow(v[0], 2) + pow(v[1], 2) + pow(v[2], 2), 0.5);
	return mag;
}

vector<float> normalizeV(vector<float> v) {
	float mag = magV(v);

	if (mag == 0) {
		cout << "normalize fail" << 0;
		return v;
	}
	vector<float> norm ={ v[0]/mag, v[1]/mag, v[2]/mag };
	return norm;
}

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window, Shader &shader) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
	else if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
		inputMode = 1;
	}
	else if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
		inputMode = 2;
	}
	else if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
		inputMode = 3;
	}
	else if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
		inputMode = 4;
	}
	else if (glfwGetKey(window, GLFW_KEY_COMMA) == GLFW_PRESS) {
		inputMode = 5;
	}
	else if (glfwGetKey(window, GLFW_KEY_PERIOD) == GLFW_PRESS) {
		inputMode = 6;
	}
	else if (glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS) {
		inputMode = 7;
	}
	else if (glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
		inputMode = 8;
	}
	else if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS) {
		inputMode = 9;
	}
	else if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS) {
		inputMode = 10;
	}
	else if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS) {
		inputMode = 11;
	}
	else if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS) {
		inputMode = 12;
	}
	else if (glfwGetKey(window, GLFW_KEY_LEFT_BRACKET) == GLFW_PRESS) {
		inputMode = 13;
	}
	else if (glfwGetKey(window, GLFW_KEY_RIGHT_BRACKET) == GLFW_PRESS) {
		inputMode = 14;
	}
	else if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
		inputMode = 15;
	}
	else if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
		inputMode = 16;
	}else {
		inputMode = 0;
	}
	if (glfwGetKey(window, GLFW_KEY_SLASH) == GLFW_PRESS) {
		slashPress = true;
	}
	if (glfwGetKey(window, GLFW_KEY_SLASH) == GLFW_RELEASE) {
		if (slashPress) {
			if (proj == orthogonal) {
				cout << "Changed to perspective projection." << endl;
				proj = perspective;
			}
			else {
				cout << "Changed to orthogonal projection." << endl;
				proj = orthogonal;
			}
			slashPress = false;
		}
	}

	
	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
		spacePress = true;
	}
	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_RELEASE) {
		if (spacePress) {
			if (shape == cube) {
				shape = cylinder;
			}
			else {
				shape = cube;
			}
			spacePress = false;
		}
	}

}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

vector<vector<float>> createViewMatrix(vector<float> eye, vector<float> gaze, vector<float> top) {
	vector<vector<float>> view = matrix(matSize, matSize);
	
	vector<float>forward = eye-gaze;
	vector<float> w = normalizeV(forward);

	vector<float> cross1 = crossV(top, w);
	vector<float> u = normalizeV(cross1);

	vector<float> v = crossV(w, u);

	vector<vector<float>> mat1 = matrix(matSize, matSize);
	for (int i = 0; i < matSize - 1; i++) {
		for (int j = 0; j < matSize - 1; j++) {
			if (j == 0) {
				mat1[i][j] = u[i];
			}
			else if (j == 1) {
				mat1[i][j] = v[i];
			}
			else if (j == 2) {
				mat1[i][j] = w[i];
			}
		}
	}
	

	vector<vector<float>> mat2 = matrix(matSize, matSize);
	for (int i = 0; i < matSize - 1; i++) {
		mat2[i][3] = -eye[i];
	}

	view = mat1 * mat2;

	return view;
}

vector<vector<float>> updateView(vector<vector<float>> view) {
	/*vector<float> eye = { 0,0,-1 };
	vector<float> gaze = { 0,0,0 };
	vector<float> top = { 0,1,0 };*/

	if (inputMode == 15) { //move camera along y axis
		if (view[1][3] - 0.001f >= -1) {
			//eye[1] = view[1][3]-0.001f;
			//view = createViewMatrix(eye, gaze, top);
			vector<float> v = { 0,-0.001f,0 };
			view = translateM(view, v);
		}
	}
	else if (inputMode == 16) {
		if (view[1][3] + 0.001f <= 1) {
			//eye[1] = view[1][3]+0.001f;
			//view = createViewMatrix(eye, gaze, top);
			vector<float> v = { 0,0.001f,0 };
			view = translateM(view, v);
		}
	}
	
	//view = setupViewMatrix(eye,gaze,top);
	//view = createViewMatrix(eye, gaze, top);
	return view;
}

vector<vector<float>> createProjMatrix( float xleft, float xright, float ybot, float ytop, float znear, float zfar) {
	
	vector <vector<float>> projM = matrix(matSize, matSize);

	if (proj == orthogonal) {
		projM[0][0] = 2 / (xright - xleft);
		projM[1][1] = 2 / (ytop - ybot);
		projM[2][2] = -2 / (zfar - znear);
		
		projM[3][0] = -(xright + xleft) / (xright - xleft);
		projM[3][1] = -(ytop + ybot) / (ytop - ybot);
		projM[3][2] = -(zfar + znear) / (zfar - znear);
	}
	else if (proj == perspective) {
		//projM[0][0] = (2*abs(znear)) / (xright - xleft);
		projM[0][0] = (2 * znear) / (xright - xleft);
		//projM[1][1] = (2 * abs(znear)) / (ytop - ybot);
		projM[1][1] = (2 * znear) / (ytop - ybot);

		projM[2][0] = (xright + xleft) / (xright - xleft);
		projM[2][1] = (ytop + ybot) / (ytop - ybot);
		projM[2][2] = -(zfar+znear) / (zfar - znear);
		//projM[2][2] = -(abs(zfar) + abs(znear)) / (abs(zfar) - abs(znear));

		projM[2][3] = -1;
		//projM[3][2] = -(2 * abs(zfar)*abs(znear)) / (abs(zfar) - abs(znear));
		projM[3][2] = -(2*zfar*znear) / (zfar - znear);
		projM[3][3] = 0;
	}
	

	return projM;
}

vector<vector<float>> updateModel(vector<vector<float>> model) {
	vector<float> origin = { -model[0][3],-model[1][3],-model[2][3] };
	vector<float> position = { model[0][3],model[1][3],model[2][3] };

	if (inputMode == 0) {//do nothing
	}
	else if (inputMode == 1) {//translate in negative X
		vector<float> v = { -0.001f,0,0 };
		model = translateM(model, v);
	}
	else if (inputMode == 2) {//translate in positive X
		vector<float> v = { 0.001f,0,0 };
		model = translateM(model, v);
	}
	else if (inputMode == 3) {//translate in negative Y
		vector<float> v = { 0, -0.001f,0 };
		model = translateM(model, v);
	}
	else if (inputMode == 4) {//translate in positive Y
		vector<float> v = { 0, 0.001f,0 };
		model = translateM(model, v);
	}
	else if (inputMode == 5) {//translate in negative Z
		vector<float> v = { 0,0, -0.001f };
		model = translateM(model, v);
	}
	else if (inputMode == 6) {//translate in positive Z
		vector<float> v = { 0, 0, 0.001f };
		model = translateM(model, v);
	}
	else if (inputMode == 7) {//scale negatively
		vector<float> v = { 0.9999f, 0.9999f, 0.9999f };
		model = translateM(model, origin);
		model = scaleM(model, v);
		model = translateM(model, position);
	}
	else if (inputMode == 8) {//scale positively
		vector<float> v = { 1.0001f, 1.0001f, 1.0001f };
		model = translateM(model, origin);
		model = scaleM(model, v);
		model = translateM(model, position);
	}
	else if (inputMode == 9) {//rotate negatively around x
		model = translateM(model, origin);
		model = rotateM(model, -.001f, 'x');
		model = translateM(model, position);
	}
	else if (inputMode == 10) {//rotate positively around x
		model = translateM(model, origin);
		model = rotateM(model, .001f, 'x');
		model = translateM(model, position);
	}
	else if (inputMode == 11) {//rotate negatively around y
		model = translateM(model, origin);
		model = rotateM(model, -.001f, 'y');
		model = translateM(model, position);
	}
	else if (inputMode == 12) {//rotate positively around y
		model = translateM(model, origin);
		model = rotateM(model, .001f, 'y');
		model = translateM(model, position);
	}
	else if (inputMode == 13) {//rotate negatively around z
		model = translateM(model, origin);
		model = rotateM(model, -.001f, 'z');
		model = translateM(model, position);
	}
	else if (inputMode == 14) {//rotate positively around z
		model = translateM(model, origin);
		model = rotateM(model, .001f, 'z');
		model = translateM(model, position);
	}
	
	return model;
}

vector<vector<float>> drawCircle(vector<float> start, float nSides, float radius)
{
	vector<vector<float>> circ;
	float nVert = nSides+2;
	
	circ.push_back(start);

	for (int i = 1; i < nVert; i++) {
		vector<float> point;
		point.push_back(start[0] + (cos(i *  (2.0f*PI) / nSides) * radius));
		point.push_back(start[1]);
		point.push_back(start[2] + (sin(i * (2.0f*PI) / nSides) * radius));
		point.push_back(0);//rcolor
		point.push_back(1);//gcolor
		point.push_back(0);//bcolor

		circ.push_back(point);
	}
	return circ;
}

float** convertM(vector<vector<float>> m) {
	float** convM = 0;
	convM = new float*[matSize];

	for (int h = 0; h < matSize; h++)
	{
		convM[h] = new float[matSize];
		for (int w = 0; w < matSize; w++)
		{
			convM[h][w] = m[h][w];
		}
	}

	return convM;
}

int main(void) {

    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);u
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 4", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    // init the cube model
    float cubeVert[] = {
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 1.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 1.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,

        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f
    };


	//create cylinder model
	vector<float> point1 = { 0,0,0,0,1,0 };
	const int numSides = 360;
	vector<vector<float>> circ = drawCircle(point1, numSides, 0.5f);
	float circVert[numSides+2][6];
	for (int i = 0; i < circ.size(); i++) {
		for (int j = 0; j < circ[i].size(); j++) {
			circVert[i][j] = circ[i][j];
		}
	}

	//create wrapping triangle model for cylinder
	vector<vector<vector<float>>> wrap;
	float cylheight = 0.5f;
	//for each point on circle create two triangles
	for (int i = 0; i < circ.size(); i++) {
	//	
			//triangle 1
			/*vector<vector<float>> tri1;
			vector<float> t1p1 = { circ[i][0] ,circ[i][1],circ[i][2],1,1,1 };
			vector<float> t1p2 = { circ[i][0] ,circ[i][1] + cylheight, circ[i][2],1,1,1 };
			
			vector<float> t1p3;
			if (i == circ.size() - 1) {
				t1p3.insert(t1p3.end(), { circ[0][0] ,circ[0][1], circ[0][2],1,1,1 });
			}
			else {
				t1p3.insert(t1p3.end(), {circ[i + 1][0] ,circ[i + 1][1], circ[i + 1][2],1,1,1 });
			}
			tri1.push_back(t1p1);
			tri1.push_back(t1p2);
			tri1.push_back(t1p3);

			wrap.push_back(tri1);*/

	//		//triangle 2
	//		vector<vector<float>> tri2;
	//		vector<float> t2p1 = { circ[i][0] ,circ[i + 1][1] + cylheight , circ[i + 1][2],1,1,1 };

	//		vector<float> t2p2;
	//		if (i == circ.size() - 1) {
	//			t2p2.insert(t2p2.end(), { circ[0][0] ,circ[0][1], circ[0][2],1,1,1 });
	//		}
	//		else {
	//			t2p2.insert(t2p2.end(), { circ[i + 1][0] ,circ[i + 1][1], circ[i + 1][2],1,1,1 });
	//		}

	//		vector<float> t2p3;
	//		if (i == circ.size() - 1) {
	//			t2p3.insert(t2p3.end(), { circ[0][0] ,circ[0][1] + cylheight, circ[0][2],1,1,1 });
	//		}
	//		else {
	//			t2p3.insert(t2p3.end(), { circ[i + 1][0] ,circ[i + 1][1] + cylheight, circ[i + 1][2],1,1,1 });
	//		}
	//		tri2.push_back(t2p1);
	//		tri2.push_back(t2p2);
	//		tri2.push_back(t2p3);

	//		wrap.push_back(tri2);
	}

	//triangle to be used in cylinder wrap
	float wrapVert[] = {
		0.5f,  0.5f, 0.0f, 1.0, 1.0, 1.0,
		0.5f, -0.5f, 0.0f, 1.0, 1.0, 1.0,
		-0.5f,  0.5f, 0.0f, 1.0, 1.0, 1.0,

	};
	/*float wrapVert[(numSides + 2)][2][6];
	for (int i = 0; i < wrap.size(); i++) {
		for (int j = 0; j < wrap[i].size(); j++) {
			for (int k = 0; k < wrap[i][j].size(); k++) {
				wrapVert[i][j][k] = wrap[i][j][k];
			}
		}
	}*/

	//CUBE
    // copy vertex data
    GLuint cubeVBO;
    glGenBuffers(1, &cubeVBO);
    glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVert), cubeVert, GL_STATIC_DRAW);

    // describe vertex layout
    GLuint cubeVAO;
    glGenVertexArrays(1, &cubeVAO);
    glBindVertexArray(cubeVAO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float), (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

	//TRIANGLE WRAP
	// copy vertex data
	GLuint triVBO;
	glGenBuffers(1, &triVBO);
	glBindBuffer(GL_ARRAY_BUFFER, triVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(wrapVert), wrapVert, GL_STATIC_DRAW);

	// describe vertex layout
	GLuint triVAO;
	glGenVertexArrays(1, &triVAO);
	glBindVertexArray(triVAO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	////CIRCLE
	// copy vertex data
	GLuint circVBO;
	glGenBuffers(1, &circVBO);
	glBindBuffer(GL_ARRAY_BUFFER, circVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(circVert), circVert, GL_STATIC_DRAW);

	// describe vertex layout
	GLuint circVAO;
	glGenVertexArrays(1, &circVAO);
	glBindVertexArray(circVAO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);


    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    // setup the textures
    shader.use();

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

	//create view matrix
	vector<float> eye = {0,0,-1};
	vector<float> gaze = {0,0,0};
	vector<float> top = {0,1,0};
	vector<vector<float>> view = createViewMatrix(eye,gaze,top);

	//create model matrix
	vector<vector<float>> model = matrix(matSize, matSize);

    /* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window)) {

		// process input
		processInput(window, shader);

		/* Render here */
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// activate shader
		shader.use();

		//update the transformation of model
		model = updateModel(model);

		//update the transformation of model
		view = updateView(view);

		//create projection matrix
		float aspectRatio = (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT;
		float xleft = -aspectRatio;
		float xright = aspectRatio;
		float ybot = -1.0f;
		float ytop = 1.0f;
		float znear = .1f;
		float zfar = 100.0f;

		vector<vector<float>> projM = createProjMatrix(xleft, xright, ybot, ytop, znear, zfar);

		//convert matrix using vectors, to matrix with contiguous data
		//needed to be done in order to be passed to vertex shader properly
		float convTransf[matSize][matSize];
		float convView[matSize][matSize];
		float convProj[matSize][matSize];

		for (int i = 0; i < model.size(); i++) {
			for (int j = 0; j < model[i].size(); j++) {
				convTransf[i][j] = model[i][j];
				convView[i][j] = view[i][j];
				convProj[i][j] = projM[i][j];
			}
		}


		//update uniform model matrix
		unsigned int transformLoc = glGetUniformLocation(shader.id(), "transf");
		glUniformMatrix4fv(transformLoc, 1, GL_TRUE, &convTransf[0][0]);

		//update uniform view matrix
		unsigned int viewLoc = glGetUniformLocation(shader.id(), "view");
		glUniformMatrix4fv(viewLoc, 1, GL_TRUE, &convView[0][0]);

		//update uniform projection matrix
		unsigned int projLoc = glGetUniformLocation(shader.id(), "projection");
		glUniformMatrix4fv(projLoc, 1, GL_TRUE, &convProj[0][0]);

		// render the cube

		if (shape == cube) {
			glBindVertexArray(cubeVAO);
			glDrawArrays(GL_TRIANGLES, 0, sizeof(cubeVert));
		}
		if (shape == cylinder) {
			//position of circles
			vector<float> vec1{ 0, 0, 0 };
			vector<float> vec2{ 0, 0.5, 0 };
			vector<float> vec3{ 0, -0.5, 0 };

			glBindVertexArray(circVAO);
			//bottom circle
			glDrawArrays(GL_TRIANGLE_FAN, 0, sizeof(circVert));
			
			//top circle
			model = translateM(model, vec2);
			for (int j = 0; j < model.size(); j++) {
				for (int k = 0; k < model[j].size(); k++) {
					convTransf[j][k] = model[j][k];
				}
			}
			unsigned int transformLoc = glGetUniformLocation(shader.id(), "transf");
			glUniformMatrix4fv(transformLoc, 1, GL_TRUE, &convTransf[0][0]);
			glDrawArrays(GL_TRIANGLE_FAN, 0, sizeof(circVert));

			//revert translation so bottom can be rendered
			model = translateM(model, vec3);

			//draw triangles for cylinder
			//glBindVertexArray(triVAO);
			//glDrawArrays(GL_TRIANGLES, 0, sizeof(wrapVert));
			
		}
		

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
