#include <iostream>

#include "bitmap_image.hpp"

//to contain a point and its rgb value
struct point{
  float x;
  float y;
  int r;
  int g;
  int b;
};

int main(int argc, char** argv) {
    /*
      Prompt user for 3 points separated by whitespace.

      Part 1:
          You'll need to get the x and y coordinate as floating point values
          from the user for 3 points that make up a triangle.

      Part 3:
          You'll need to also request 3 colors from the user each having
          3 floating point values (red, green and blue) that range from 0 to 1.
    */

  //contain all points of triangle
  point points[3];

  //get three x,y points and rgb values
  std::cout << "Enter 3 x,y points and r,g,b values:" << std::endl;

  for (int i = 0; i < 3; i++){
    bool done = false;
    while(not done){
      std::cout << "Point " << i+1 << " : "<< std::endl;
      std::cout << "x: ";
      std::cin >> points[i].x;
      std::cout << "y: ";
      std::cin >> points[i].y;
      std::cout << "r: ";
      std::cin >> points[i].r;
      std::cout << "g: ";
      std::cin >> points[i].g;
      std::cout << "b: ";
      std::cin >> points[i].b;

      if(points[i].r < 0 or points[i].r > 1 or points[i].g < 0 or points[i].g > 1 or points[i].b < 0 or points[i].b > 1 ){
        std::cout << "RGB values have to be a value between 0 and 1" << std::endl;
      } else {
        done = true;
      }
    } 
  }

  //find max width/height values for bordering rectangle
  float maxX = 0;
  float maxY = 0;
  for (int i = 0; i < 3; i++){
    if(points[i].x > maxX){
      maxX = points[i].x;
    }
    if(points[i].y > maxY){
      maxY = points[i].y;
    }
  }

  //creating bitmap image to fit to triangle
  std::cout << "Created bitmap_image (" << maxX << ", " << maxY << ")" << std::endl;
  bitmap_image image(maxX, maxY);

  //coloring all pixels white
  // rgb_t white = make_colour(255, 255, 255);

    //take determinant of equations for barycentric coordinates
    //L1*X1 + L2*X2 + L3*X3 = X4
    //L1*Y1 + L2*Y2 + L3*Y3 = Y4
    // L1   + L2    + L3    = 1 

  // perform cramers rule on matrix A  
  // A = X1 X2 X3
  //     Y1 Y2 Y3
  //     1  1  1

  float detA = ((points[0].x*points[1].y) - (points[0].x*points[2].y)
    - (points[1].x*points[0].y) + (points[1].x*points[2].y)
    + (points[2].x*points[0].y) - (points[2].x*points[1].y));

  if (detA != 0){ //divisor is not 0

    //iterate through each pixel
    for(int i = 0; i < maxX; i++){
      for(int j = 0; j < maxY; j++){

        float L1 = ((i*points[1].y) - (i*points[2].y)
          - (points[1].x*j) + (points[1].x*points[2].y)
          + (points[2].x*j) - (points[2].x*points[1].y));

        float L2 = ((points[0].x*j) - (points[0].x*points[2].y)
          - (i*points[0].y) + (i*points[2].y)
          + (points[2].x*points[0].y) - (points[2].x*j));

        float L3 = ((points[0].x*points[1].y) - (points[0].x*j)
          - (points[1].x*points[0].y) + (points[1].x*j)
          + (i*points[0].y) - (i*points[1].y));

        L1 = L1/detA;
        L2 = L2/detA;
        L3 = L3/detA;

        //need to see if all barcentric coordinates are positive, then the
        //point is within the triangle
        if (L1 > 0 and L2 > 0 and L3 > 0){

          //each color is a linear combination of the points color and 
          //the value that color is considered in the barycentric coordinates
          //of the triangle
          float r1 = 0, g1 = 0, b1 = 0;
          r1 += L1*points[0].r + L2*points[1].r + L3*points[2].r;
          g1 += L1*points[0].g + L2*points[1].g + L3*points[2].g;
          b1 += L1*points[0].b + L2*points[1].b + L3*points[2].b;

          //convert to range 0-255
          r1 = r1*255;
          g1 = g1*255;
          b1 = b1*255;

          //color pixel in triangle accordingly
          rgb_t color = make_colour(r1, g1, b1);
          image.set_pixel(i,j,color);
        }
      }
    }
  } 
    /*
      Part 1:
          Calculate the bounding box of the 3 provided points and loop
          over each pixel in that box and set it to white using:

          rgb_t color = make_color(255, 255, 255);
          image.set_pixel(x,y,color);

      Part 2:
          Modify your loop from part 1. Using barycentric coordinates,
          determine if the pixel lies within the triangle defined by
          the 3 provided points. If it is color it white, otherwise
          move on to the next pixel.

      Part 3:
          For each pixel in the triangle, calculate the color based on
          the calculated barycentric coordinates and the 3 provided
          colors. Your colors should have been entered as floating point
          numbers from 0 to 1. The red, green and blue components range
          from 0 to 255. Be sure to make the conversion.
    */

    //save image to a file
    image.save_image("triangle.bmp");
    std::cout << "Success" << std::endl;
  }
