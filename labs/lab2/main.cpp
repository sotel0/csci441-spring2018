#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

struct Point { float x; float y; };

Point w2nd(float x, float y);
void requestTriangleInput();

const int tri_length = 15;
int SCR_WIDTH = 640;
int SCR_HEIGHT = 480;
GLfloat triangleData[tri_length];


/**
 * BELOW IS A BUNCH OF HELPER CODE
 * You do not need to understand what is going on with it, but if you want to
 * know, let me know and I can walk you through it.
 */

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

GLFWwindow* initWindow() {
    GLFWwindow* window;
    if (!glfwInit()) {
        return NULL;
    }

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Lab 2", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return NULL;
    }

    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return NULL;
    }

    return window;
}

std::string shaderTypeName(GLenum shaderType) {
    switch(shaderType) {
        case GL_VERTEX_SHADER: return "VERTEX";
        case GL_FRAGMENT_SHADER: return "FRAGMENT";
        default: return "UNKNOWN";
    }
}

std::string readFile(const std::string& fileName) {
//std::string readFile(std::string fileName) {
    std::ifstream stream(fileName);
    std::stringstream buffer;
    buffer << stream.rdbuf();

    std::string source = buffer.str();
    std::cout << "Source:" << std::endl;
    std::cout << source << std::endl;

    return source;
}

/** END OF CODE THAT YOU DON'T NEED TO WORRY ABOUT */
GLuint createShader(const std::string& fileName, GLenum shaderType) {

//GLuint createShader(std::string fileName, GLenum shaderType) {
    std::string source = readFile(fileName);
    const char* src_ptr = source.c_str();

    /** YOU WILL ADD CODE STARTING HERE */

	//get shader ID
	GLuint shader = glCreateShader(shaderType);

	//attach the source code of the shader
	glShaderSource(shader, 1, &src_ptr, NULL);
	
	//compile shader
	glCompileShader(shader);
    
	// create the shader using
    // glCreateShader, glShaderSource, and glCompileShader
    /** END CODE HERE */

    // Perform some simple error handling on the shader
    int success;
    char infoLog[512];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        std::cerr << "ERROR::SHADER::" << shaderTypeName(shaderType)
            <<"::COMPILATION_FAILED\n"
            << infoLog << std::endl;
    }

    return shader;
}

GLuint createShaderProgram(GLuint vertexShader, GLuint fragmentShader) {
    /** YOU WILL ADD CODE STARTING HERE */
    // create the program using glCreateProgram, glAttachShader, glLinkProgram
    GLuint program = glCreateProgram();

	//attach shaders to program
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);

	//link program
	glLinkProgram(program);
    /** END CODE HERE */

    // Perform some simple error handling
    int success;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        char infoLog[512];
        glGetProgramInfoLog(program, 512, NULL, infoLog);
        std::cerr << "ERROR::PROGRAM::COMPILATION_FAILED\n"
            << infoLog << std::endl;
    }

    return program;
}

int main(void) {

	GLFWwindow* window = initWindow();
	if (!window) {
		std::cout << "There was an error setting up the window" << std::endl;
		return 1;
	}

	/** YOU WILL ADD DATA INITIALIZATION CODE STARTING HERE */

	/* PART1: ask the user for coordinates and colors, and convert to normalized
	 * device coordinates */

	 // convert the triangle to an array of floats containing
	 // normalized device coordinates and color components.
	 // float triangle[] = ...
	 //--------------------------------

	 //this function requests input for 3 points, and converts the coordinates
	 //global array of floats in=s modified 
	requestTriangleInput();
	
	//---------------------------------
    /** PART2: map the data */

    // create vertex and array buffer objects using
    // glGenBuffers, glGenVertexArrays
	
    GLuint VBO, VAO;
	glGenBuffers(1, &VBO);
	glGenVertexArrays(1, &VAO);

    // setup triangle using glBindVertexArray, glBindBuffer, GlBufferData
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleData), triangleData, GL_STATIC_DRAW);


	//bind Vertex Array Object
	glBindVertexArray(VAO);


    // setup the attribute pointer for the coordinates
    // setup the attribute pointer for the colors
    // both will use glVertexAttribPointer and glEnableVertexAttribArray;
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(2*sizeof(float)));
	glEnableVertexAttribArray(1);


    /** PART3: create the shader program */

    // create the shaders
    // YOU WILL HAVE TO ADD CODE TO THE createShader FUNCTION ABOVE
	
	//the files could not be found because the executing directory is in a cmake build location
	GLuint vertexShader = createShader("C:\\Users\\IS\\Desktop\\GRAPHICS2\\csci441-spring2018\\labs\\lab2\\vert.glsl", GL_VERTEX_SHADER);
	GLuint fragmentShader = createShader("C:\\Users\\IS\\Desktop\\GRAPHICS2\\csci441-spring2018\\labs\\lab2\\frag.glsl", GL_FRAGMENT_SHADER);
	//GLuint vertexShader = createShader("..\vert.glsl", GL_VERTEX_SHADER);
	//GLuint fragmentShader = createShader("..\frag.glsl", GL_FRAGMENT_SHADER);


	// create the shader program
    // YOU WILL HAVE TO ADD CODE TO THE createShaderProgram FUNCTION ABOVE
    GLuint shaderProgram = createShaderProgram(vertexShader, fragmentShader);

    // cleanup the vertex and fragment shaders using glDeleteShader
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

    /** END INITIALIZATION CODE */

    while (!glfwWindowShouldClose(window)) {
        // you don't need to worry about processInput, all it does is listen
        // for the escape character and terminate when escape is pressed.
        processInput(window);

        /** YOU WILL ADD RENDERING CODE STARTING HERE */
        /** PART4: Implemting the rendering loop */

        // clear the screen with your favorite color using glClearColor
		glClearColor(0.7f, 0.7f, 0.7f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

        // set the shader program using glUseProgram
		glUseProgram(shaderProgram);

        // bind the vertex array using glBindVertexArray
		glBindVertexArray(VAO);
		
        // draw the triangles using glDrawArrays
		glDrawArrays(GL_TRIANGLES, 0, 3);

        /** END RENDERING CODE */

        // Swap front and back buffers
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}

Point w2nd(float x, float y) {

	Point p = { 0,0 };

	//check if beyond screen size
	if (x > SCR_WIDTH || y > SCR_HEIGHT) {
		std::cout << "fail point conversion, x > SCR_WIDTH or y > SCR_HEIGHT" << std::endl;

	}
	//check to prevent zero division
	else if (SCR_WIDTH == 0 || SCR_HEIGHT == 0) {
		std::cout << "fail point conversion, SCR_WIDTH or SCR_HEIGHT = 0" << std::endl;
	}
	//nomralize coordinates
	else {
		p.x = -1 + (x * (2 / (float)SCR_WIDTH));
		p.y = -1 + (y * (2 / (float)SCR_HEIGHT));
		//p.y = 1 - (y * (2 / (float)SCR_HEIGHT));
	}
	return p;
}

void requestTriangleInput() {
	int numPoints = 3;	
	std::string input;	//user input
	int numInputs = 5;	//number of inputs for one point

	for (int p = 0; p < numPoints; p++) {
		bool done = false;	//to end while loop for input
		while (!done) {

			std::cout << "Enter point " << p+1 << " as 'x,y,r,g,b'" << std::endl;
			std::cin >> input;
			std::stringstream ss(input);
			std::vector<std::string> result;
			std::vector<float> result2;

			//parse string based on comma
			while (ss.good()){
				std::string substr;
				getline(ss, substr, ',');
				result.push_back(substr);
			}

			//check if input data is required length
			if (result.size() != numInputs) {
				std::cout << "Need five values." << std::endl;
				continue;
			}

			//convert each string point attribute to float
			//add to new float vector
			for (int i = 0; i < numInputs; i++) {
				//std::cout << std::stof(result[i]) << std::endl;
				result2.push_back(std::stof(result[i]));
			}

			//make range of r,g,b to be >=0 and <=1
			if (result2[2] < 0 || result2[2] > 1 || result2[3] < 0 || result2[3] > 1 || result2[4] < 0 || result2[4] > 1) {
				std::cout << "r,g,b values need to be between 0 and 1 inclusively." << std::endl;
				continue;
			}

			//convert to normalized device coordinates
			Point coordinate = w2nd(result2[0], result2[1]);
			result2[0] = coordinate.x;
			result2[1] = coordinate.y;

			//place in vector array
			for (int i = 0; i < numInputs; i++) {
				//which point * numInputs per inputs + which input
				triangleData[p*numInputs + i] = result2[i];
			}

			//finish one point
			done = true;
		}
	}
	
}
