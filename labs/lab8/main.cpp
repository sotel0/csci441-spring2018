#include <iostream>

#include <glm/glm.hpp>

#include "bitmap_image.hpp"

glm::vec3 normalizeV3(const glm::vec3& vector);

enum ViewType { orthographic, perspective };
const int imageWidth = 640;
const int imageHeight = 480;

struct Viewport {
    glm::vec2 min;
    glm::vec2 max;

    Viewport(const glm::vec2& min, const glm::vec2& max)
        : min(min), max(max) {}
};

struct Ray {
	glm::vec3 origin;
	glm::vec3 direction;

	Ray(const glm::vec3& origin, const glm::vec3& direction)
		: origin(origin), direction(normalizeV3(direction)) {
	}
};

struct Sphere {
    int id;
    glm::vec3 center;
    glm::vec3 color;
    float radius;

    Sphere(const glm::vec3& center=glm::vec3(0,0,0),
            float radius=0,
            const glm::vec3& color=glm::vec3(0,0,0))
        : center(center), radius(radius), color(color) {
            static int id_seed = 0;
            id = ++id_seed;
        }
};
glm::vec3 normalizeV3(const glm::vec3& vector) {
	float mag = pow(pow(vector[0], 2) + pow(vector[1], 2) + pow(vector[2], 2), 0.5);
	if (mag == 0) {
		return vector;
	}
	glm::vec3 norm = vector / mag;
	return norm;
}

float sign(float f) {
	if (f > 0) {
		return 1.0f;
	}
	else if (f < 0) {
		return -1.0f;
	}
	else {
		return 0.0f;
	}
}

bool checkSphereIntersect(Ray ray, Sphere sphere) {
	bool intersected = false;

	//solve the quadratic equation to see if sphere and ray intersect
	//A x^2 + B x + C = 0
	float A = glm::dot(ray.direction, ray.direction);
	float B = 2 * glm::dot(ray.direction, ray.origin - sphere.center);
	float C = glm::dot(ray.origin - sphere.center, ray.origin - sphere.center) - (sphere.radius*sphere.radius);
	
	float D = pow(B, 2) - 4*A*C;
	
	float Q = -1 * (B + sign(B) * sqrt(D));

	float x1 = Q / (2 * A);
	float x2 = (2 * C) / Q;
	
	if (D >= 0) {
		intersected = true;
		return intersected;
	}
	return intersected;
}

void render(bitmap_image& image, const std::vector<Sphere>& world) {
    // TODO: implement ray tracer

	Viewport view = Viewport(glm::vec2(-5,5), glm::vec2(5, -5));
	rgb_t color;
	float camDistance = 5;
	glm::vec3 camPos = glm::vec3(0, 0, -camDistance);
	
	ViewType vt = orthographic;
	//ViewType vt = perspective;
	

	for (int i = 0; i < imageWidth; i++) {
		for (int j = 0; j < imageHeight; j++) {

			//create the origin and direction vectors
			glm::vec3 origin;
			glm::vec3 direction;

			float ui = view.min[0] + (view.max[0] - view.min[0])*(i + .5) / imageWidth;
			float vj = view.min[1] + (view.max[1] - view.min[1])*(j + .5) / imageHeight;

			if (vt == orthographic) {
				origin = glm::vec3(ui, vj, 0);
				direction = glm::vec3(0, 0, -1);
			}
			else if (vt == perspective) {
				origin = camPos;
				direction = glm::vec3(ui, vj, 0) - camPos;
			}

			//create a ray at this point
			Ray ray = Ray(origin, direction);
			

			//check if ray intersects sphere
			for (int k = 0; k < world.size(); k++) {
				bool intersected = checkSphereIntersect(ray, world[k]);
				if (intersected) {
					//if it does create new colour
					color = make_colour(world[k].color[0]*255, world[k].color[1]*255, world[k].color[2]*255);
					break;
				}
				else {
					//set the image background to this color
					color = make_colour(75, 15, 211);
				}
			}

			//set color to pixel
			image.set_pixel(i,j,color);
		}
	}

}



int main(int argc, char** argv) {

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(imageWidth,imageHeight);



    // build world
    std::vector<Sphere> world = {
        Sphere(glm::vec3(0, 0, 1), 1, glm::vec3(1,1,0)),
        Sphere(glm::vec3(1, 2, 4), 2, glm::vec3(0,1,1)),
        Sphere(glm::vec3(2, 2, 6), 3, glm::vec3(1,0,1)),
		Sphere(glm::vec3(2, 3, 3), 1, glm::vec3(1,0,0)),
		Sphere(glm::vec3(-1, 2, 1), 2, glm::vec3(1,1,1)),
		Sphere(glm::vec3(2, -1, 8), 5, glm::vec3(0,0,0)),
    };

    // render the world
    render(image, world);

    image.save_image("../ray-traced.bmp");
    std::cout << "Success" << std::endl;
}


