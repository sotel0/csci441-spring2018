#version 330 core
in vec3 ourColor;
in vec3 FragPos; 
in vec3 Normal;

out vec4 fragColor;

uniform vec4 lightColor;
uniform vec4 lightPos;
uniform mat4 camera;
uniform float shinniness;

void main() {

	//ambient shading
    vec3 amb = 0.2 * ourColor;
	vec3 ambient = amb * vec3(lightColor);

	//get camera position
	vec3 norm = normalize(Normal);
	//mat4 invCam = inverse(camera);
	vec3 cameraPos = {camera[3][0],camera[3][1],camera[3][2]};

	//transform light position
	//vec3 lightPos2 = vec3(camera*lightPos);
	vec3 lightPos2 = vec3(lightPos);

	//vectors in lighting system
	vec3 lightDir = normalize(lightPos2 - FragPos);  
	vec3 viewDir = normalize(cameraPos - FragPos);
	vec3 reflectDir = reflect(-lightDir,norm);

	//diffuse shading
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * vec3(lightColor);

	//specular shading
	float spec = pow(max(dot(viewDir, reflectDir), 0.0),shinniness);
	vec3 specular = spec * vec3(lightColor);

	//apply to vertex color
	vec3 result = (ambient + diffuse + specular) * ourColor;
	
	//update frag color
	fragColor = vec4(result, 1.0f);

}
