#ifndef _CSCI441_PARSER_H_
#define _CSCI441_PARSER_H_

#include <cstdlib>
#include <vector>
#include <string>
#include <fstream>
#include "vectorOps.h"
using std::vector;


class ObjParser {

public:
	vector<float> coords;
	vector<vector<float>> vertices;
	vector<vector<int>> faces;
	vector <vector<float>> normals; //normals for each corresponding vertex
	float r = 0;
	float g = 0;
	float b = 0;
	
	ObjParser(const std::string &s, float red, float green, float blue) {
		r = red;
		g = green;
		b = blue;
		
		//add path to file name
		filepath = "../models/" + s;

		//attempt to read file
		read = readFile();
		
		if (read) {
			//create default normals
			createNormVar();

			//calc normals
			calcFlatNorms(); 

		}

	}
	void createNormVar() {
		for (int i = 0; i < vertices.size();i++) {
			vector<float> v = { 1,0,0 };
			normals.push_back(v);
		}
		recreateCoords();
	}

	void calcSmoothNorms() {
		if (read) {
			for (int i = 0; i < vertices.size();i++) {
				vector<int> f;//adjacent faces

				//find connected faces
				for (int j = 0;j < faces.size();j++) {
					for (int k = 0; k < faces[j].size();k++) {
						if (i == faces[j][k]) { //if vertex is in face
							f.push_back(j); //save face
						}
					}
				}

				vector<float> avgN;
				for (int j = 0; j < f.size();j++) {
					//calculate face normal, of adjacent faces
					vector<float> norm;
					norm = crossV(vertices[faces[f[j]][0]], vertices[faces[f[j]][1]]);
					norm = normalizeV(norm);

					if (j == 0) {
						avgN = norm;
					}
					else {
						vector<float> temp = { avgN[0] + norm[0], avgN[1] + norm[1], avgN[2] + norm[2] };
						avgN = temp;
					}
				}
				//continue average
				vector<float> temp = { avgN[0] / f.size(),  avgN[1] / f.size(),  avgN[2] / f.size() };
				avgN = temp;

				//update normals to be average normals
				normals[i] = avgN;

				//recreate coords
				recreateCoords();
			}
		}
	}
	void calcFlatNorms() {
		if (read) {
			for (int i = 0; i < faces.size();i++) {
				//calculate face normal
				vector<float> norm;
				norm = crossV(vertices[faces[i][0]], vertices[faces[i][1]]);
				norm = normalizeV(norm);

				//assign normal to each of those vectors
				for (int j = 0; j < faces[i].size();j++) {
					normals[faces[i][j]] = norm;
				}
			}
			//recreate coords
			recreateCoords();
		}
	}

	void recreateCoords() {
		vector<float> temp;
		
		//order from the faces
		for (int i = 0; i < faces.size();i++) {
			for (int j = 0;j < faces[i].size();j++) {
				//gets vertices of the face
				for (int k = 0; k < vertices[0].size();k++) {
					temp.push_back(vertices[faces[i][j]][k]);
				}
				//add color values
				temp.push_back(r);
				temp.push_back(g);
				temp.push_back(b);

				for (int k = 0; k < vertices[0].size();k++) {
					temp.push_back(normals[faces[i][j]][k]);
				}
			}
		}

		//for (int i = 0;i < vertices.size();i++) {
		//	//add vertices coordinates
		//	for (int j = 0; j < vertices[i].size();j++) {
		//		temp.push_back(vertices[i][j]);
		//	}
		//	//add color values
		//	temp.push_back(r);
		//	temp.push_back(g);
		//	temp.push_back(b);

		//	//add normals
		//	for (int j = 0;j < normals[i].size();j++) {
		//		temp.push_back(normals[i][j]);
		//	}
		//}

		coords = temp;
	}

private:
	bool read = false;
	std::string filepath;

	bool readFile() {
		//create file from path
		FILE * file = fopen(filepath.c_str(), "r");
		if (file == NULL) {
			printf("File failed to open !\n");
			return false;
		}
		else { //parse file

			while (true) {
				char header[150];

				int type = fscanf(file, "%s", header);
				if (type == EOF) {
					break;
				}else if (strcmp(header, "f") == 0) {
					//indices are at +1
					vector<int> v;
					int v1, v2, v3;
					fscanf(file, "%d %d %d\n", &v1, &v2, &v3);
					v.push_back(v1-1);
					v.push_back(v2-1);
					v.push_back(v3-1);
					faces.push_back(v);

				}
				else if (strcmp(header, "v") == 0) {
					vector<float> v;
					float vx, vy, vz;
					fscanf(file, "%f %f %f\n", &vx, &vy, &vz);
					v.push_back(vx);
					v.push_back(vy);
					v.push_back(vz);
					vertices.push_back(v);
				}
				else if (strcmp(header, "#") == 0) {
					//do nothing
				}
			}
			//std::ifstream input(filepath);
			//std::string line;
			/////////create list of faces? list of vertices? Yes?
			///////create coords, yes to make Matrix

			////////make some of these their own functions
			///////add vertices to coords

			//////add default color vals to coords

			///////calculate normals
			//----------------------


			//while (std::getline(input, line)) {
			//	if (line[0] == 'f') {

			//	}
			//	else if (line[0] == 'v') {

			//	}
			//	else if (line[0] == '#') {
			//		std::cout << line << std::endl;
			//	} 
			//	//std::cout << line << std::endl;
			//}
		
			//std::string line;
			//while (!file.eof())
			//{
			//	getline(file, line);

				// then do what you need to do

			//}


			//while (std::getline(file,str)) {
			//	std::cout << str << std::endl;
				//char lineHeader[128];
				// read the first word of the line
				//int res = fscanf(file, "%s", lineHeader);
				//if (res == EOF)
				//	break; // EOF = End Of File. Quit the loop.
				//else {
				//	std::cout << lineHeader << std::endl;
				//}
			//}
		}
	}

};
#endif