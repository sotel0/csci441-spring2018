Isaac Sotelo
Program 1 - 2D Shape Modeler
=========
###Notification###
After a while the console window was bugging out, 
but inputs were still being recieved. There just was not any console output,
which makes it hard to use the app.
Restarting the application worked fixed it.
###Notification###


To swap between modes, press : (in application window)
'V' for view mode or
'E' for edit mode. 
The default is view mode.

View Mode
=========
Hold the mouse button 1 and drag to move viewing area.
Once in view mode, press : (in application window)
	'-' to zoom out
	'=' to zoom in

Edit Mode
=========
Edit mode defaults to '1'
Once in edit mode, the sub-modes can be accessed by pressing : (in application window)

STAMP MODE
'1' for stamp mode
	The stamp defaults to 't' 
	Once in stamp mode, to create a stamp press : (in application window)
		't' to select triangle stamps
		's' to select square stamps
		'c' to select circle stamps
		--something for primitive
			type two floats separated by a comma : (in console)
				(window defaults to view of 1.5x1.5)
				Input coordinate of object as 'x,y'
				Input scaling of object as 'x,y'
		
USER DEFINE PRIMITIVE MODE
'2' for user defined primitive mode
	Once in user defined mode press: (in application window)
		'u' to start creating the user shape
			Type in how many points there will be, at least 3 points (in console)
			Input coordinates as 'x,y'
			Now the shape can be stamped in STAMP mode

GROUP MODE
'3' for group mode
	Once in group mode press: (in application window)
		'g' - to start selection of a group
			Enter how many shapes to be grouped.
			Enter which shapes, 1..5. (enter one at a time)
		'u' - to select a group to ungroup
			Enter how many shapes to be ungrouped.
			Enter which shapes, 1..5. (enter one at a time)

MODIFY MODE
'4' for modify mode
	Once in modify mode press : (in application window)
		'o' to specify the origin, defaults to(0,0)
		'/' to cycle between objects [defaults to 1st object]

		"left arrow" key to move object left
		"right arrow" key to move object right
		"up arrow" key to move object up
		"down arrow" key to move object down
		'-' to scale object down in size
		'=' to scale object up in size
		'[' to rotate object left
		']' to rotate object right

		'c' to start color input : (in console)
			Input color as 'r,g,b'

The shape I made was a hummingbird, they are neat. The shape seems superior because
it is made up of other primitives. It also has a lopsided center of gravity if that
will be used at all later on.