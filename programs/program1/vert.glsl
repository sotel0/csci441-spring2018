#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;

out vec3 ourColor;
uniform mat4 transf;
uniform mat4 cam;
uniform mat4 projection;

uniform vec3 colorChange;

void main() {
	//mat4 MVP = view*projection*cam*transf;

    gl_Position = projection*cam*transf*vec4(aPos, 1.0);

    ourColor = colorChange*aColor;
}
