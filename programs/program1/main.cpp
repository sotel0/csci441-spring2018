/*
csci441 Graphics Program 1 - 2D Shape Modeler
Isaac Sotelo
March 08 2018
*/

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <vector>

#define PI 3.1415927

using std::vector;using std::cout;using std::cin;using std::endl;

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int matSize = 4; //max size of matrix


enum curMode { view, edit };
enum eMode {stamp,userPrim,group,modify}; 
enum eMode1 { square, circle, triangle, userShape };
enum eMode4 {
	doNothing, pickColor, moveLeft, moveRight, moveUp, moveDown,
	scaleUp, scaleDown, rotateL, rotateR, camUp, camDown
};
enum projection { orthogonal,perspective };
projection proj = orthogonal;

curMode mode = view;
eMode editMode = stamp;
eMode1 editMode1 = triangle;
eMode4 editMode4 = doNothing;

//contain shapes created in program
struct createdShape { vector<vector<float>> transf; eMode1 shape; vector<float> color = {1,1,1}; };
vector<vector<createdShape>> drawnShapes;
int selectedShape; //index
int sizeOfUserShape; //in bits

//camera parameters
vector<vector<float>> cam;
float sensitivity = 0.01f; //mouse drag view
vector<vector<float>> projM; //projection

vector<float> transfOrigin = { 0,0,0 };//for modify mode
vector<float> mouseClickPos = { 0,0 };
int btnPressed = 0;

GLuint sqrVBO;
GLuint sqrVAO;
GLuint triVBO;
GLuint triVAO;
GLuint circVBO;
GLuint circVAO;
GLuint userVBO;
GLuint userVAO;

GLuint shaderID;

//init the triangle model
float triVert[] = {
	0.5f,  0.5f, 0.0f, 1.0, 1.0, 1.0,
	0.5f, -0.5f, 0.0f, 1.0, 1.0, 1.0,
	-0.5f,  0.5f, 0.0f, 1.0, 1.0, 1.0,
};

// init the square model
float sqrVert[] = {
	-0.5f, -0.5f,  0.0f,  1.0f, 1.0f, 1.0f,
	0.5f, -0.5f,  0.0f,  1.0f, 1.0f, 1.0f,
	0.5f,  0.5f,  0.0f,  1.0f, 1.0f, 1.0f,
	0.5f,  0.5f,  0.0f,  1.0f, 1.0f, 1.0f,
	-0.5f,  0.5f,  0.0f,  1.0f, 1.0f, 1.0f,
	-0.5f, -0.5f,  0.0f,  1.0f, 1.0f, 1.0f,
};

//user created model
vector<float> userVert;
// circle model
vector<float> circVert;

//========================================//
//matrix or vector prototype operations
void createUserVertices(vector<vector<float>> userShape);
vector<vector<float>> matrix(int rows, int cols);
std::ostream& operator<< (std::ostream &os, const vector<float> v);
std::ostream& operator<< (std::ostream &os, const vector<vector<float>> m);
vector<vector<float>> createProjMatrix(float xleft, float xright, float ybot, float ytop, float znear, float zfar);
vector<vector<float>> operator*(const vector<vector<float>>& m1, const vector<vector<float>>& m2);
vector<vector<float>> operator-(const vector<vector<float>>& m1, const vector<vector<float>>& m2);
vector<vector<float>> operator+(const vector<vector<float>>& m1, const vector<vector<float>>& m2);
vector<float> operator-(const vector<float>& m1, const vector<float>& m2);
vector<vector<float>> scaleM(vector<vector<float>> m, vector<float> scale);
vector<vector<float>> rotateM(vector<vector<float>> m, float degree, char axis);
vector<vector<float>> updateModel(vector<vector<float>> model);
vector<vector<float>> translateM(vector<vector<float>> m, vector<float> translation);
vector<float> crossV(vector<float> a, vector<float> b);
vector<float> normalizeV(vector<float> v);
vector<float> w2nd(vector<float> v);
float magV(vector<float> v);
void printM(vector<vector<float>> m);
void printV(vector<float> v);

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
	//float aspectRatio = (float)SCREEN_WIDTH / SCREEN_HEIGHT;
    //glViewport(0, 0, width*aspectRatio, height*aspectRatio);
	//float aspectRatio = (float)width / height;
	//glViewport(0, 0, width*aspectRatio, height*aspectRatio);
	//float cx, halfWidth = width * 0.5f;
	//float aspect = (float)width / (float)height;

	//adjust projection matrix when screen change
	float aspectRatio = (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT;
	float xleft = -aspectRatio*width;
	float xright = aspectRatio*height;
	float ybot = -5.0f;
	float ytop = 5.0f;
	float znear = .1f;
	float zfar = 100.0f;

	projM = createProjMatrix(xleft, xright, ybot, ytop, znear, zfar);
	GLint vp[4]; 
	glGetIntegerv(GL_VIEWPORT, vp);
	float oldAspc = (float)vp[0] / (float)vp[1];
	glViewport(0, 0, width*oldAspc, height*oldAspc);
}

vector<float> getInputs(int numInputs, bool pad) {
	//pad : whether to add zeros up to vector size = 3

	bool done = false;
	vector<float> vec;
	if (numInputs == 1) { //returns 1 value
		while (done == false) {
			if (cin.fail()) {
				cin.clear();
				cin.ignore(100, '\n'); // it will ignore 100 characters or get to the end of the line.
			}
			vec.clear();
			std::string input;
			cin >> input;
				try {
					vec.push_back(stof(input));
					done = true;
				}
				catch (...) {
					cout<< "Please enter a single value:" << endl;
					done = false;
				}
		}
	} 
	else if (numInputs == 2){ //returns three values, sets z=0
		while (done == false) {
			if (cin.fail()) {
				cin.clear();
				cin.ignore(100, '\n'); // it will ignore 100 characters or get to the end of the line.
			}

			vec.clear();
			std::string input;
			cin >> input;

			//parse string based on comma, convert to float
			std::stringstream ss(input);
			while (ss.good()) {
				std::string substr;
				getline(ss, substr, ',');
				try {
					vec.push_back(stof(substr));
					done = true;
				}
				catch (...) {
					done = false;
				}
			}
			if (vec.size() != 2) {
				done = false;
				cout << "Please enter 2 floats separated by a comma:" << endl;
			}
			if (done && pad) {
				//add the z value
				vec.push_back(0.0f);
			}
		}
	}
	else if (numInputs == 3) {//returns three values
		while (done == false) {
			if (cin.fail()) {
				cin.clear();
				cin.ignore(100, '\n'); // it will ignore 100 characters or get to the end of the line.
			}
			vec.clear();
			std::string input;
			cin >> input;

			//parse string based on comma, convert to float
			std::stringstream ss(input);
			while (ss.good()) {
				std::string substr;
				getline(ss, substr, ',');
				try {
					vec.push_back(stof(substr));
					done = true;
				}
				catch (...) {
					done = false;
				}
			}
			if (vec.size() != 3) {
				done = false;
				cout << "Please enter 3 floats separated by a comma:" << endl;
			} else if (vec[0] < 0 || vec[0] > 1 || vec[1] < 0 || vec[1] > 1 || vec[2] < 0 || vec[2] > 1) {
				done = false;
				cout << "Please enter r,g,b as values between 0.0 and 1.0:" << endl;
			}
		}
	}
	return vec;
}

void renderShapes() {
	//iterate through list of transformations
	for (int i = 0; i < drawnShapes.size();i++){
		for (int j = 0; j < drawnShapes[i].size();j++) { //for groups
		//draw each transformation 

		//convert to be passed to uniform variable
			float convTransf[matSize][matSize];
			for (int k = 0; k < drawnShapes[i][j].transf.size(); k++) {
				for (int l = 0; l < drawnShapes[i][j].transf[j].size(); l++) {
					convTransf[k][l] = drawnShapes[i][j].transf[k][l];
				}
			}

			//update uniform model matrix
			unsigned int transformLoc = glGetUniformLocation(shaderID, "transf");
			glUniformMatrix4fv(transformLoc, 1, GL_TRUE, &convTransf[0][0]);

			//update uniform color
			unsigned int colorLoc = glGetUniformLocation(shaderID, "colorChange");
			glUniform3f(colorLoc, drawnShapes[i][j].color[0], drawnShapes[i][j].color[1], drawnShapes[i][j].color[2]);

			if (drawnShapes[i][j].shape == triangle) {
				glBindVertexArray(triVAO);
				glDrawArrays(GL_TRIANGLES, 0, sizeof(triVert));
			}
			else if (drawnShapes[i][j].shape == square) {
				glBindVertexArray(sqrVAO);
				glDrawArrays(GL_TRIANGLES, 0, sizeof(sqrVert));
			}
			else if (drawnShapes[i][j].shape == circle) {
				glBindVertexArray(circVAO);
				glDrawArrays(GL_TRIANGLE_FAN, 0, circVert.size() * sizeof(float));
			}
			else if (drawnShapes[i][j].shape == userShape) {
				glBindVertexArray(userVAO);
				glDrawArrays(GL_TRIANGLE_FAN, 0, userVert.size() * sizeof(float));
			}
		}
	}
}

void stampShape(Shader shader) {

	std::vector<float> location;
	std::vector<float> scale;
	std::vector<float> rotate;

	//get shape LOCATION
	cout << "Enter shape location as x,y :" << endl;
	location = getInputs(2,true);
	
	//get shape SCALE
	cout << "Enter shape scale as x,y :" << endl;
	scale = getInputs(2,true);
	
	//get the ROTATION
	cout << "Enter the degree of rotation as a float value :" << endl;
	rotate = getInputs(1,false);

	//Scale then Translate
	vector<vector<float>> transf = matrix(matSize, matSize);
	transf = rotateM(transf, rotate[0], 'z');
	transf = scaleM(transf, scale);
	transf = translateM(transf, location);

	createdShape s;

	if (editMode1 == triangle) {
		//create struct to hold shape
		s = { transf, triangle };
	}
	else if (editMode1 == circle) {
		//create struct to hold shape
		s = { transf, circle };
	}
	else if (editMode1 == square) {
		//create struct to hold shape
		s = { transf, square };
	}
	else if (editMode1 == userShape) {
		//create struct to hold shape
		s = { transf, userShape };
	}

	//add to list of shapes
	vector<createdShape> cs = { s };
	drawnShapes.push_back(cs);
}
void unGroup(int index) {
	for (int i = 0; i < drawnShapes[index].size(); i++) {
		//add shapes in group to back of list
		vector<createdShape> sub = { drawnShapes[index][i] };
		drawnShapes.push_back(sub);
	}
	//remove old group from list
	drawnShapes.erase(drawnShapes.begin() + index);
}
void createGroup(vector<float> indexList) {
	//index needs to be -1
	vector<vector<createdShape>> temp;
	vector<createdShape> group;
	vector<int> ind;
	//cleanup indexList
	for (int i = 0; i < indexList.size(); i++) {
		ind.push_back((int)indexList[i]-1);
	}

	//reform the list of shapes
	for (int i = 0; i < drawnShapes.size();i++) {
		//if this is one of the indexes to be grouped
		if (std::find(ind.begin(), ind.end(), i) != ind.end()) {
			for (int j = 0; j < drawnShapes[i].size();j++) { //for sub groups
				group.push_back(drawnShapes[i][j]);
			}
		}
		else {
			temp.push_back(drawnShapes[i]);
		}
	}
	//add group to new list
	temp.push_back(group);

	//replace old list with new list
	drawnShapes = temp;
}
void updateShapeTransf() {
	//if theres a group update the whole group's transforms
	for (int i = 0; i < drawnShapes[selectedShape].size(); i++) {
		drawnShapes[selectedShape][i].transf = updateModel(drawnShapes[selectedShape][i].transf);
	}
}

void processInput(GLFWwindow *window, Shader &shader) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
	if (glfwGetKey(window, GLFW_KEY_V) == GLFW_PRESS) {
		btnPressed = 1;
	}
	else if (glfwGetKey(window, GLFW_KEY_V) == GLFW_RELEASE) {
		if (btnPressed == 1) {
			mode = view;
			cout << "Now in VIEW mode" << endl;
			btnPressed = 0;
		}
	}

	if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) {
		btnPressed = 2;
	}
	else if (glfwGetKey(window, GLFW_KEY_E) == GLFW_RELEASE) {
		if (btnPressed == 2) {
			mode = edit;
			cout << "Now in EDIT mode" << endl;
			btnPressed = 0;
		}
	}


	if (mode == view) {
		if (glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS) {
			vector<float> v = {0,0,-.001f};
			cam = translateM(cam, v);
		}
		else if (glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
			vector<float> v = { 0,0,.001f };
			cam = translateM(cam, v);
		}
	}
	else if (mode == edit) {
		//swap which edit mode
		if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS) { 
			btnPressed = 3;
		}
		else if (glfwGetKey(window, GLFW_KEY_1) == GLFW_RELEASE) {
			if (btnPressed == 3) {
				editMode = stamp;
				cout << "Now in STAMP mode, press shape 'key' while in app window" << endl;
				btnPressed = 0;
			}
		}

		if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS) {
			btnPressed = 4;
		}
		else if (glfwGetKey(window, GLFW_KEY_2) == GLFW_RELEASE) {
			if (btnPressed == 4) {
				editMode = userPrim;
				cout << "Now in User Primitive mode" << endl;
				btnPressed = 0;
			}
		}

		if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS) {
			btnPressed = 5;
		}
		else if (glfwGetKey(window, GLFW_KEY_3) == GLFW_RELEASE) {
			if (btnPressed == 5) {
				editMode = group;
				cout << "Now in GROUP mode" << endl;
				btnPressed = 0;
			}
		}
		
		if (glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS) {
			btnPressed = 6;

		}
		else if (glfwGetKey(window, GLFW_KEY_4) == GLFW_RELEASE) {
			if (btnPressed == 6) {
				editMode = modify;
				cout << "Now in MODIFY mode" << endl;
				if (drawnShapes.size() == 0) {
					cout << "No shapes to modify, please stamp a shape or create one" << endl;
				}
				cout << "Cycle given shapes with '/'" << endl;
				cout << "First shape is auto selected" << endl;
				cout << "" << endl;
				selectedShape = 0;
				vector<float> o = { 0,0,0 };
				transfOrigin = o;
				
				btnPressed = 0;
			}
		}
		

		//specifc edit mode
		if (editMode == stamp) {
			if (glfwGetKey(window, GLFW_KEY_T) == GLFW_PRESS) {
				btnPressed = 10;
			}
			else if (glfwGetKey(window, GLFW_KEY_T) == GLFW_RELEASE) {
				if (btnPressed == 10) {
					editMode1 = triangle;
					cout << "selected TRIANGLE stamp" << endl;
					stampShape(shader);
					btnPressed = 0;
				}
			}
			if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
				btnPressed = 11;
			}
			else if (glfwGetKey(window, GLFW_KEY_S) == GLFW_RELEASE) {
				if (btnPressed == 11) {
					editMode1 = square;
					cout << "selected SQUARE stamp" << endl;
					stampShape(shader);
					btnPressed = 0;
				}
			}
			if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS) {
				btnPressed = 12;
			}
			else if (glfwGetKey(window, GLFW_KEY_C) == GLFW_RELEASE) {
				if (btnPressed == 12) {
					editMode1 = circle;
					cout << "selected CIRCLE stamp" << endl;
					stampShape(shader);
					btnPressed = 0;
				}
			}
			if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS) {
				btnPressed = 13;
			}
			else if (glfwGetKey(window, GLFW_KEY_U) == GLFW_RELEASE) {
				if (btnPressed == 13) {
					editMode1 = userShape;
					cout << "selected USER SHAPE stamp" << endl;
					stampShape(shader);
					btnPressed = 0;
				}
			}
			
		}
		else if (editMode == group) {
			if (glfwGetKey(window, GLFW_KEY_G) == GLFW_PRESS) {
				btnPressed = 20;
			}
			else if (glfwGetKey(window, GLFW_KEY_G) == GLFW_RELEASE) {
				if (btnPressed == 20) {
					//show list of shapes with # next to them
					cout << "Current Shapes" << endl;
					if (drawnShapes.empty()) {
						cout << "NO SHAPES EXIST" << endl;
					}
					for (int i = 0; i < drawnShapes.size(); i++) {
						cout << i + 1 << " : ";
						for (int j = 0; j < drawnShapes[i].size();j++) {
							if (drawnShapes[i][j].shape == triangle) {
								cout << " triangle ";
							} else if (drawnShapes[i][j].shape == circle) {
								cout << " circle ";
							}
							else if (drawnShapes[i][j].shape == square) {
								cout << " square ";
							}
							else if (drawnShapes[i][j].shape == userPrim) {
								cout << " user_shape ";
							}
						}
						cout << endl;
					}
					//question how many shapes
					cout << "How many of these shapes will you use?" << endl;
					bool done = false;
					float num;
					vector<float> values;
					while (!done) {
						num = getInputs(1, false)[0];
						if (num < 1|| num > drawnShapes.size()) {
							cout << "Please enter a value from 1 to " << drawnShapes.size() << endl;
						}
						else {
							done = true;
						}
					}
					//have user enter one number at a time
					cout << "Now please enter each value one at a time." << endl;
					for (int i = 0; i < num; i++) {
						done = false;
						while (!done) {
							float f = getInputs(1, false)[0];
							if (f < 1 || f > drawnShapes.size()) {
								cout << "Please enter a value from 1 to " + drawnShapes.size() << endl;
							}
							else {
								done = true;
								values.push_back(f);
							}
						}
					}

					//create group from indexes
					createGroup(values);
					cout << "The shapes have been GROUPED." << endl;
					btnPressed = 0;
				}
			}
			if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS) {
				btnPressed = 21;
			}
			else if (glfwGetKey(window, GLFW_KEY_U) == GLFW_RELEASE) {
				if (btnPressed == 21) {
					//display the groups available
					int count = 0;
					vector<int> indexes;

					cout << "List of current grouped shapes." << endl;
					for (int i = 0; i < drawnShapes.size(); i++) {
						if (drawnShapes[i].size() > 1) {
							indexes.push_back(i);
							cout << count + 1<< " : ";
							count++;
							for (int j = 0; j < drawnShapes[i].size();j++) {
								if (drawnShapes[i][j].shape == triangle) {
									cout << " triangle ";
								}
								else if (drawnShapes[i][j].shape == circle) {
									cout << " circle ";
								}
								else if (drawnShapes[i][j].shape == square) {
									cout << " square ";
								}
								else if (drawnShapes[i][j].shape == userPrim) {
									cout << " user_shape ";
								}
							}
							cout << endl;
						}
					}
					if (count == 0) {
						cout << "NO GROUPS TO UNGROUP" << endl;
					}
					else {
						cout << "Which group do you want to ungroup?" << endl;
						bool done = false;
						float num;
						while (!done) {
							num = getInputs(1, false)[0];
							if (num < 1 || num >= (count+1)) {
								cout << "Please enter a value from 1 to " << count+1 << endl;
							}
							else {
								done = true;
							}
						}
						//ungroup selected group
						int g = indexes[num - 1];
						unGroup(g);
						cout << "The shapes have been UNGROUPED." << endl;
					}
					
					btnPressed = 0;
				}
			}
		}
		else if (editMode == userPrim) {
			if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS) {
				btnPressed = 30;
			}
			else if (glfwGetKey(window, GLFW_KEY_U) == GLFW_RELEASE) {
				if (btnPressed == 30) {
					bool created = false;
					int numPoints = 0;
					vector<vector<float>> userShape;
					cout << "How many points will there be? (at least 3)" << endl;
					while (numPoints < 3) {
						cin >> numPoints;
						if (cin.fail()) {
							cin.clear();
							cin.ignore(100, '\n'); // it will ignore 100 characters or get to the end of the line.
						}
						
						if (numPoints < 3) {
							cout << "Input at least 3 points." << endl;
							continue;
						}
					}
					cout << "Input coordinates as x,y :" << endl;
					for(int i = 0; i < numPoints; i++){
						vector<float> v = getInputs(2, true);
						v.push_back(1.0f); //rcolor
						v.push_back(1.0f); //gcolor
						v.push_back(1.0f); //bcolor
						userShape.push_back(v);
					}
					//setup vertices in GPU
					createUserVertices(userShape);
					cout << "saved USER created SHAPE" << endl;
					btnPressed = 0;
				}
			}

		}
		else if (editMode == modify) {
			//swap to select shape
			if (glfwGetKey(window, GLFW_KEY_SLASH) == GLFW_PRESS) {
				btnPressed = 40;
			} 
			else if (glfwGetKey(window, GLFW_KEY_SLASH) == GLFW_RELEASE) {
				if (btnPressed == 40) {
					if (drawnShapes.size() > 0) {
						if (selectedShape + 1 == drawnShapes.size()) {
							selectedShape = 0;
						}
						else {
							selectedShape++;
						}
						cout << "Selected object changed." << endl;
					}
					else {
						cout << "Please create/stamp a shape first" << endl;
					}
					btnPressed = 0;
				}
			}

			if (drawnShapes.size() > 0) {
				
				if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS) {
					btnPressed = 41;
				}
				else if (glfwGetKey(window, GLFW_KEY_O) == GLFW_RELEASE) {
					if (btnPressed == 41) {
						cout << "Specify origin of transformations as x,y :" << endl;
						transfOrigin = getInputs(2, true);
						btnPressed = 0;
					}
				}
				if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS) {
					btnPressed = 42;
				}
				else if (glfwGetKey(window, GLFW_KEY_C) == GLFW_RELEASE) {
					if (btnPressed == 42) {

						cout << "Please enter values between 0.0 and 1.0 in the format r,g,b :" << endl;
						editMode4 = pickColor;
						vector<float> c = getInputs(3, false);

						for (int i = 0; i < drawnShapes[selectedShape].size(); i++) {
							drawnShapes[selectedShape][i].color = c;
						}

						btnPressed = 0;

					}
				}

				if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
					editMode4 = moveLeft;
					updateShapeTransf();
				}
				else if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
					editMode4 = moveRight;
					updateShapeTransf();
				}
				else if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
					editMode4 = moveDown;
					updateShapeTransf();
				}
				else if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
					editMode4 = moveUp;
					updateShapeTransf();
				}
				else if (glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS) {
					editMode4 = scaleDown;
					updateShapeTransf();
				}
				else if (glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
					editMode4 = scaleUp;
					updateShapeTransf();
				}
				else if (glfwGetKey(window, GLFW_KEY_LEFT_BRACKET) == GLFW_PRESS) {
					editMode4 = rotateL;
					updateShapeTransf();
				}
				else if (glfwGetKey(window, GLFW_KEY_RIGHT_BRACKET) == GLFW_PRESS) {
					editMode4 = rotateR;
					updateShapeTransf();
				}
				else {
					editMode4 = doNothing;
				}
			}
		}
	}
}


void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

vector<vector<float>> createViewPortMatrix() {
	vector<vector<float>> view = matrix(matSize, matSize);

	//create viewport matrix
	for (int i = 0; i < matSize; i++) {
		for (int j = 0; j < matSize; j++) {
			if (i== 0 && j == 0) {
				view[i][j] = SCREEN_WIDTH / 2;
			}
			else if (i == 1 && j == 1) {
				view[i][j] = SCREEN_HEIGHT / 2;
			}
			else if (i == 0 && j == 3) {
				view[i][j] = (SCREEN_WIDTH -1) / 2;
			}
			else if (i == 1 && j == 3) {
				view[i][j] = (SCREEN_HEIGHT - 1) / 2;
			}
		}
	}

	return view;
}

vector<vector<float>> createCamMatrix(vector<float> eye, vector<float> gaze, vector<float> top) {
	vector<vector<float>> cam = matrix(matSize, matSize);
	
	vector<float>forward = eye-gaze;
	vector<float> w = normalizeV(forward);

	vector<float> cross1 = crossV(top, w);
	vector<float> u = normalizeV(cross1);

	vector<float> v = crossV(w, u);

	vector<vector<float>> mat1 = matrix(matSize, matSize);
	for (int i = 0; i < matSize - 1; i++) {
		for (int j = 0; j < matSize - 1; j++) {
			if (j == 0) {
				mat1[i][j] = u[i];
			}
			else if (j == 1) {
				mat1[i][j] = v[i];
			}
			else if (j == 2) {
				mat1[i][j] = w[i];
			}
		}
	}
	

	vector<vector<float>> mat2 = matrix(matSize, matSize);
	for (int i = 0; i < matSize - 1; i++) {
		mat2[i][3] = -eye[i];
	}

	cam = mat1 * mat2;

	return cam;
}

vector<vector<float>> updateModel(vector<vector<float>> model) {
	vector<float> pos2origin = { -model[0][3],-model[1][3],-model[2][3] };
	vector<float> position = { model[0][3],model[1][3],model[2][3] };
	vector<float> transf2origin = {-transfOrigin[0],-transfOrigin[1] ,-transfOrigin[2] };

	if (editMode4 == doNothing) {//do nothing
	}
	else if (editMode4 == moveLeft) {//translate in negative X
		vector<float> v = { -0.001f,0,0 };
		model = translateM(model, v);
	}
	else if (editMode4 == moveRight) {//translate in positive X
		vector<float> v = { 0.001f,0,0 };
		model = translateM(model, v);
	}
	else if (editMode4 == moveDown) {//translate in negative Y
		vector<float> v = { 0, -0.001f,0 };
		model = translateM(model, v);
	}
	else if (editMode4 == moveUp) {//translate in positive Y
		vector<float> v = { 0, 0.001f,0 };
		model = translateM(model, v);
	}
	else if (editMode4 == scaleDown) {//scale negatively
		vector<float> v = { 0.9999f, 0.9999f, 0.9999f };
		model = translateM(model, pos2origin);
		model = translateM(model, transfOrigin);
		model = scaleM(model, v);
		model = translateM(model, transf2origin);
		model = translateM(model, position);
	}
	else if (editMode4 == scaleUp) {//scale positively
		vector<float> v = { 1.0001f, 1.0001f, 1.0001f };
		model = translateM(model, pos2origin);
		model = translateM(model, transfOrigin);
		model = scaleM(model, v);
		model = translateM(model, transf2origin);
		model = translateM(model, position);
	}
	else if (editMode4 == rotateL) {//rotate negatively around z
		model = translateM(model, pos2origin);
		model = translateM(model, transfOrigin);
		model = rotateM(model, .001f, 'z');
		model = translateM(model, transf2origin);
		model = translateM(model, position);
	}
	else if (editMode4 == rotateR) {//rotate positively around z
		model = translateM(model, pos2origin);
		model = translateM(model, transfOrigin);
		model = rotateM(model, -.001f, 'z');
		model = translateM(model, transf2origin);
		model = translateM(model, position);
	}
	
	return model;
}

vector<vector<float>> drawCircle(vector<float> start, float nSides, float radius)
{
	vector<vector<float>> circ;
	float nVert = nSides+2;
	
	circ.push_back(start);

	for (int i = 1; i < nVert; i++) {
		vector<float> point;
		point.push_back(start[0] + (cos(i *  (2.0f*PI) / nSides) * radius));
		point.push_back(start[1] + (sin(i * (2.0f*PI) / nSides) * radius));
		point.push_back(start[2]);
		point.push_back(1);//rcolor
		point.push_back(1);//gcolor
		point.push_back(1);//bcolor

		circ.push_back(point);
	}
	return circ;
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos) {
	if (mode == view) {
		
		if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS) {
			
			vector<float> coord = { (float)xpos,(float)ypos };
			coord = coord - mouseClickPos; //get direction vector

			coord.push_back(0);   //z coordinate
			coord = normalizeV(coord);

			coord[1] = -coord[1] * sensitivity; //reverse so dragging works
			coord[0] = coord[0] * sensitivity; 

			cam = translateM(cam, coord);

			//save previous click so can calculate direction
			mouseClickPos[0] = xpos;
			mouseClickPos[1] = ypos;
				
		}
		if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_RELEASE) {
		}
	}
}

void createUserVertices(vector<vector<float>> userShape) {
	//when creating a new shape
	userVert.clear();

	//create user shape model
	for (int i = 0; i < userShape.size(); i++) {
		for (int j = 0; j < userShape[i].size(); j++) {
			userVert.push_back(userShape[i][j]);
		}
	}

	//USER SHAPE
	// copy vertex data
	glGenBuffers(1, &userVBO);
	glBindBuffer(GL_ARRAY_BUFFER, userVBO);
	glBufferData(GL_ARRAY_BUFFER, userVert.size()*sizeof(float), &userVert[0], GL_STATIC_DRAW);

	// describe vertex layout
	glGenVertexArrays(1, &userVAO);
	glBindVertexArray(userVAO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);
}

void vertexHandler() {

	//create circle model
	vector<float> circPoint = { 0,0,0,1,1,1 };
	vector<vector<float>> circ = drawCircle(circPoint, 360, 0.5f);
	for (int i = 0; i < circ.size(); i++) {
		for (int j = 0; j < circ[i].size(); j++) {
			circVert.push_back(circ[i][j]);
		}
	}

	//CUBE
	// copy vertex data
	glGenBuffers(1, &sqrVBO);
	glBindBuffer(GL_ARRAY_BUFFER, sqrVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sqrVert), sqrVert, GL_STATIC_DRAW);

	// describe vertex layout
	glGenVertexArrays(1, &sqrVAO);
	glBindVertexArray(sqrVAO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	//TRIANGLE WRAP
	// copy vertex data
	glGenBuffers(1, &triVBO);
	glBindBuffer(GL_ARRAY_BUFFER, triVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triVert), triVert, GL_STATIC_DRAW);

	// describe vertex layout
	glGenVertexArrays(1, &triVAO);
	glBindVertexArray(triVAO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	////CIRCLE
	// copy vertex data
	glGenBuffers(1, &circVBO);
	glBindBuffer(GL_ARRAY_BUFFER, circVBO);
	glBufferData(GL_ARRAY_BUFFER, circVert.size()*sizeof(float), &circVert[0], GL_STATIC_DRAW);

	// describe vertex layout
	glGenVertexArrays(1, &circVAO);
	glBindVertexArray(circVAO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);
}

int main(void) {

    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);u
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 4", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

	//retrieving cursor input 
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	glfwSetCursorPosCallback(window, mouse_callback);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

	//handle shape vertex objects and data
	vertexHandler();

	// create the shaders
	Shader shader("../vert.glsl", "../frag.glsl");
	shaderID = shader.id();

    // setup the textures
    shader.use();

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

	//init colorChange
	float color[3] = { 1,1,1 };
	unsigned int colorLoc = glGetUniformLocation(shaderID, "colorChange");
	glUniform3f(colorLoc, color[0], color[1], color[2]);

	//create cam matrix
	vector<float> eye = { 0,0,1 };
	vector<float> gaze = { 0,0,0 };
	vector<float> top = { 0,1,0 };
	cam = createCamMatrix(eye, gaze, top);
	
	//create model matrix
	vector<vector<float>> model = matrix(matSize, matSize);

	//create projection matrix
	float aspectRatio = (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT;
	float xleft = -aspectRatio;
	float xright = aspectRatio;
	float ybot = -1.0f;
	float ytop = 1.0f;
	float znear = .1f;
	float zfar = 100.0f;

	
	
    /* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window)) {

		// process input
		processInput(window, shader);
		
		// activate shader
		shader.use();

		/* Render here */
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		//================================================

		//update projection 
		projM = createProjMatrix(xleft, xright, ybot, ytop, znear, zfar);

		//convert matrix using vectors, to matrix with contiguous data
		//needed to be done in order to be passed to vertex shader properly
		float convTransf[matSize][matSize];
		float convCam[matSize][matSize];
		float convProj[matSize][matSize];

		for (int i = 0; i < model.size(); i++) {
			for (int j = 0; j < model[i].size(); j++) {
				convTransf[i][j] = model[i][j];
				convCam[i][j] = cam[i][j];
				convProj[i][j] = projM[i][j];
			}
		}

		////update uniform cam matrix
		unsigned int camLoc = glGetUniformLocation(shaderID, "cam");
		glUniformMatrix4fv(camLoc, 1, GL_TRUE, &convCam[0][0]);

		////update uniform model matrix
		unsigned int transformLoc = glGetUniformLocation(shaderID, "transf");
		glUniformMatrix4fv(transformLoc, 1, GL_TRUE, &convTransf[0][0]);	

		//update uniform projection matrix
		unsigned int projLoc = glGetUniformLocation(shader.id(), "projection");
		glUniformMatrix4fv(projLoc, 1, GL_TRUE, &convProj[0][0]);
		//=============================================
		// render the shape
		renderShapes();
		//glBindVertexArray(triVAO);
		//glDrawArrays(GL_TRIANGLES, 0, sizeof(triVert));

		//glBindVertexArray(sqrVAO);
		//glDrawArrays(GL_TRIANGLES, 0, sizeof(sqrVert));
		

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}

vector<float> w2nd(vector<float> v) {
	//input v is a vector with an x,y coordinate, maybe z
	if (v.size() >= 2) {
		//check if beyond screen size
		if (v[0] > SCREEN_WIDTH || v[1] > SCREEN_HEIGHT) {
			std::cout << "fail point conversion, x > SCR_WIDTH or y > SCR_HEIGHT" << std::endl;

		}
		//check to prevent zero division
		else if (SCREEN_WIDTH == 0 || SCREEN_HEIGHT == 0) {
			std::cout << "fail point conversion, SCR_WIDTH or SCR_HEIGHT = 0" << std::endl;
		}
		//nomralize coordinates
		else {
			v[0] = -1 + (v[0] * (2 / (float)SCREEN_WIDTH));
			v[1] = -1 + (v[1] * (2 / (float)SCREEN_HEIGHT));
		}
	}
	return v;
}

//printing a matrix
std::ostream& operator<< (std::ostream &os, const vector<vector<float>> m)
{
	for (int i = 0; i < m.size(); i++) {
		for (int j = 0; j < m[i].size(); j++) {
			os << " " << m[i][j] << " ";
		}
		os << endl;
	}
	return os;
}

//printing vectors
std::ostream& operator<< (std::ostream &os, const vector<float> v)
{
	for (int i = 0; i < v.size(); i++) {
		os << " " << v[i] << " ";
	}
	os << endl;
	return os;
}

//creates an identity matrix
vector<vector<float>> matrix(int rows, int cols) {
	vector<vector<float>> m;

	for (int i = 0; i < rows; i++) {
		//create new row
		vector<float> v;
		m.push_back(v);
		for (int j = 0; j < cols; j++) {
			//1's along diagonal
			if (i == j) {
				m[i].push_back(1.0f);
			}
			else {
				m[i].push_back(0.0f);
			}
		}
	}
	return m;

}
//multiplying matrices
vector<vector<float>> operator*(const vector<vector<float>>& m1, const vector<vector<float>>& m2) {
	vector<vector<float>> m3 = matrix(m1.size(), m2[0].size());

	//if matrices are wrong size for multiplication
	if (m1[0].size() != m2.size()) {
		cout << "matrix multiplication fail" << endl;
		return m3;
	}

	for (int i = 0; i < m1.size(); i++) {
		for (int j = 0; j < m2[0].size(); j++) {
			float sum = 0;
			for (int k = 0; k < m2.size(); k++) {
				sum += m1[i][k] * m2[k][j];
			}
			m3[i][j] = sum;
		}

	}
	return m3;
}

//subutracting matrices
vector<vector<float>> operator-(const vector<vector<float>>& m1, const vector<vector<float>>& m2) {
	vector<vector<float>> m3 = matrix(m1.size(), m1.size());

	if (m1.size() != m2.size() || m1[0].size() != m2[0].size()) {
		cout << "matrix subtraction fail" << endl;

		return m3;
	}

	for (int i = 0; i < m1.size(); i++) {
		for (int j = 0; j < m1[i].size(); j++) {
			m3[i][j] = m1[i][j] - m2[i][j];
		}

	}
	return m3;
}

//adding matrices
vector<vector<float>> operator+(const vector<vector<float>>& m1, const vector<vector<float>>& m2) {
	vector<vector<float>> m3 = matrix(m1.size(), m1.size());

	if (m1.size() != m2.size() || m1[0].size() != m2[0].size()) {
		cout << "matrix addition fail" << endl;

		return m3;
	}

	for (int i = 0; i < m1.size(); i++) {
		for (int j = 0; j < m1[i].size(); j++) {
			m3[i][j] = m1[i][j] + m2[i][j];
		}

	}
	return m3;
}

//subtracting two vectors
vector<float> operator-(const vector<float>& m1, const vector<float>& m2) {
	vector<float> m3 = { 0,0,0 };

	if (m1.size() != m2.size()) {
		cout << "matrix addition fail" << endl;
		return m3;
	}

	for (int i = 0; i < m1.size(); i++) {
		m3[i] = m1[i] - m2[i];
	}
	return m3;
}
//scale matrix by scaling vector amount
vector<vector<float>> scaleM(vector<vector<float>> m, vector<float> scale) {
	if (scale.size() != m.size() - 1) {
		cout << "matrix scaling fail" << endl;
		return m;
	}

	//create scaling matrix
	vector<vector<float>> s = matrix(m.size(), m.size());
	//along the diagonal except for bottom right, input scaling factors
	for (int i = 0; i < s.size() - 1; i++) {
		s[i][i] = scale[i];
	}

	//apply scaling to matrix by matrix multiplication
	m = s * m;
	return m;
}

//rotate matrix by degree amount on an axis
vector<vector<float>> rotateM(vector<vector<float>> m, float degree, char axis) {

	/*vector<float> origin = { -m[0][3],-m[1][3],-m[2][3] };
	vector<float> position = { m[0][3],m[1][3],m[2][3] };*/

	//create rotation matrix
	vector<vector<float>> r = matrix(m.size(), m.size());

	//rotate along z axis
	if (axis == 'z' || axis == 'Z') {
		for (int i = 0; i < r.size(); i++) {
			if (i == r.size() - 1) { //skip last row
				continue;
			}
			for (int j = 0; j < r[i].size(); j++) {
				if (j == r[i].size() - 1) { //skip last column
					continue;
				}
				if (i == j) {
					r[i][j] = cos(degree);
				}
				else if (i == 0 && j == 1) {
					r[i][j] = -sin(degree);
				}
				else if (i == 1 && j == 0) {
					r[i][j] = sin(degree);
				}
			}
		}
	}

	//rotate along y axis
	if (axis == 'y' || axis == 'Y') {
		for (int i = 0; i < r.size(); i++) {
			if (i == 1) { //skip second row
				continue;
			}
			for (int j = 0; j < r[i].size(); j++) {
				if (j == 1) { //skip second column
					continue;
				}
				if ((i == 0 && j == 0) || (i == 2 && j == 2)) {
					r[i][j] = cos(degree);
				}
				else if (i == 0 && j == 2) {
					r[i][j] = sin(degree);
				}
				else if (i == 2 && j == 0) {
					r[i][j] = -sin(degree);
				}
			}
		}
	}

	//rotate along x axis
	if (axis == 'x' || axis == 'X') {
		for (int i = 0; i < r.size(); i++) {
			if (i == 0) { //skip first row
				continue;
			}
			for (int j = 0; j < r[i].size(); j++) {
				if (j == 0) { //skip first column
					continue;
				}
				if ((i == 1 && j == 1) || (i == 2 && j == 2)) {
					r[i][j] = cos(degree);
				}
				else if (i == 1 && j == 2) {
					r[i][j] = -sin(degree);
				}
				else if (i == 2 && j == 1) {
					r[i][j] = sin(degree);
				}
			}
		}
	}


	//apply rotation to matrix by matrix multiplication
	m = r * m;

	return m;
}

//translate matrix by vector direction
vector<vector<float>> translateM(vector<vector<float>> m, vector<float> translation) {
	// translation: (x,y,z)
	//only translating in X and Y 
	if (translation.size() != m.size() - 1) {
		cout << "matrix translating fail" << endl;
		return m;
	}

	//create translation matrix
	vector<vector<float>> t = matrix(m.size(), m.size());

	for (int i = 0; i < translation.size(); i++) {
		t[i][3] = translation[i];
	}

	//apply translation by multiplication
	m = t * m;
	return m;
}

//cross product between vectors
vector<float> crossV(vector<float> a, vector<float> b) {
	if (b.size() != 3) {
		cout << "cross product fail" << endl;
		return a;
	}

	vector<float> cross =
	{ a[1] * b[2] - a[2] * b[1],
		a[2] * b[0] - a[0] * b[2],
		a[0] * b[1] - a[1] * b[0] };
	return cross;
}


//return a normalized vector
vector<float> normalizeV(vector<float> v) {
	float mag = pow(pow(v[0], 2) + pow(v[1], 2) + pow(v[2], 2), 0.5);

	if (mag == 0) {
		cout << "normalize fail" << 0 << endl;
		return v;
	}
	vector<float> norm = { v[0] / mag, v[1] / mag, v[2] / mag };
	return norm;
}
vector<vector<float>> createProjMatrix(float xleft, float xright, float ybot, float ytop, float znear, float zfar) {

	vector <vector<float>> projM = matrix(matSize, matSize);

	if (proj == orthogonal) {
		projM[0][0] = 2 / (xright - xleft);
		projM[1][1] = 2 / (ytop - ybot);
		projM[2][2] = -2 / (zfar - znear);

		projM[3][0] = -(xright + xleft) / (xright - xleft);
		projM[3][1] = -(ytop + ybot) / (ytop - ybot);
		projM[3][2] = -(zfar + znear) / (zfar - znear);
	}
	else if (proj == perspective) {
		//projM[0][0] = (2*abs(znear)) / (xright - xleft);
		projM[0][0] = (2 * znear) / (xright - xleft);
		//projM[1][1] = (2 * abs(znear)) / (ytop - ybot);
		projM[1][1] = (2 * znear) / (ytop - ybot);

		projM[2][0] = (xright + xleft) / (xright - xleft);
		projM[2][1] = (ytop + ybot) / (ytop - ybot);
		projM[2][2] = -(zfar + znear) / (zfar - znear);
		//projM[2][2] = -(abs(zfar) + abs(znear)) / (abs(zfar) - abs(znear));

		projM[2][3] = -1;
		//projM[3][2] = -(2 * abs(zfar)*abs(znear)) / (abs(zfar) - abs(znear));
		projM[3][2] = -(2 * zfar*znear) / (zfar - znear);
		projM[3][3] = 0;
	}


	return projM;
}

void printM(vector<vector<float>> m) {
	cout << m << endl;
}
void printV(vector<float> v) {
	cout << v << endl;
}
