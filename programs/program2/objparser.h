#ifndef _CSCI441_PARSER_H_
#define _CSCI441_PARSER_H_

#include <cstdlib>
#include <vector>
#include <string>
#include <fstream>
#include "vectorOps.h"
using std::vector;


class ObjParser {

public:
	vector<float> coords;
	vector<vector<float>> vertices;
	vector<vector<int>> faces;
	vector <vector<float>> normals; //normals for each corresponding vertex
	vector <int> normalIndices;
	float r = 0;
	float g = 0;
	float b = 0;
	
	ObjParser(const std::string &s, float red, float green, float blue) {
		r = red;
		g = green;
		b = blue;

		
		//add path to file name
		filepath = "../models/" + s;

		//attempt to read file
		read = readFile2();
		
		if (read) {
			//create default normals
			if (readNorm == false) {
				createNormVar();
			}

			//calc normals
			//calcFlatNorms(); 
			//calcSmoothNorms();
			//recreate the coords
			recreateCoords();

		}

	}
	//instantiating normals variable and recreating coords
	void createNormVar() {
		vector<vector<float>> temp;
		for (int i = 0; i < vertices.size();i++) {
			vector<float> v = { 1,0,0 };
			temp.push_back(v);
		}
		normals = temp;
	}

	//instantiating normalIndices
	void createNormIndicesVar() {
		vector<int> temp;
		for (int i = 0; i < vertices.size();i++) {
			temp.push_back(0);
		}
		normalIndices = temp;
	}

	//calculate normals for smooth shading
	void calcSmoothNorms() {
		if (read) {
			for (int i = 0; i < vertices.size();i++) {
				vector<int> f;//adjacent faces

				//find connected faces
				for (int j = 0;j < faces.size();j++) {
					for (int k = 0; k < faces[j].size();k++) {
						if (i == faces[j][k]) { //if vertex is in face
							f.push_back(j); //save face
						}
					}
				}

				vector<float> avgN;
				for (int j = 0; j < f.size();j++) {
					//calculate face normal, of adjacent faces
					vector<float> norm;
					norm = crossV(vertices[faces[f[j]][0]], vertices[faces[f[j]][1]]);
					norm = normalizeV(norm);

					if (j == 0) {
						avgN = norm;
					}
					else {
						vector<float> temp = { avgN[0] + norm[0], avgN[1] + norm[1], avgN[2] + norm[2] };
						avgN = temp;
					}
				}
				//continue average
				vector<float> temp = { avgN[0] / f.size(),  avgN[1] / f.size(),  avgN[2] / f.size() };
				avgN = temp;

				//update normals to be average normals
				normals[i] = avgN;

			}
		}
	}
	
	//calculate normals for flat shading
	void calcFlatNorms() {
		if (read) {
			for (int i = 0; i < faces.size();i++) {
				//calculate face normal
				vector<float> norm;
				norm = crossV(vertices[faces[i][0]], vertices[faces[i][1]]);
				norm = normalizeV(norm);

				//assign normal to each of those vectors
				for (int j = 0; j < faces[i].size();j++) {
					normals[faces[i][j]] = norm;
				}
			}
		}
	}

	//recreate coords container
	void recreateCoords() {
		vector<float> temp;
		
		//order from the faces
		for (int i = 0; i < faces.size();i++) {
			for (int j = 0;j < faces[i].size();j++) {
				//gets vertices of the face
				for (int k = 0; k < vertices[0].size();k++) {
					temp.push_back(vertices[faces[i][j]][k]);
				}
				//add color values
				temp.push_back((float)rand() / RAND_MAX);
				temp.push_back((float)rand() / RAND_MAX);
				temp.push_back((float)rand() / RAND_MAX);

				//add normals differently if in obj file
				if (readNorm) {
					//for the vertices add the normal
					for (int k = 0; k < normals[0].size();k++) {
						//faces[i][j] which vertex
						//normalIndices[faces[i][j]][k] which index
						//which normal value
						float t = normals[normalIndices[faces[i][j]]][k];
						temp.push_back(t);
						
					}
				}
				else { //normals has same size as vertices and is 1-1 ordered

					//add the normals
					for (int k = 0; k < vertices[0].size();k++) {
						temp.push_back(normals[faces[i][j]][k]);
					}
				}
			}
		}

		coords = temp;
	}

private:
	bool readNorm = false;
	bool read = false;
	std::string filepath;

	bool readFile2() {
		std::string line;
		std::ifstream myfile(filepath.c_str());

		if (myfile.good()) { //if file not corrupt

			//till end of file
			while (std::getline(myfile, line)) {
				
				//vertex values
				if (line.substr(0, 2) == "v ") {
					std::stringstream ss(line);
					std::string temp;
					
					vector<float> v;
					float vx, vy, vz;

					//get float values 
					for (int i = 0;i<4;i++) {
						ss >> temp;
						if (i == 0) {//header
							continue;
						}
						else if (i == 1) {
							vx = std::stof(temp, 0);
						}
						else if (i == 2) {
							vy = std::stof(temp, 0);
						}
						else if (i == 3) {
							vz = std::stof(temp, 0);
						}
					}

					//place in container
					v.push_back(vx);
					v.push_back(vy);
					v.push_back(vz);
					vertices.push_back(v);

				} 

				//vertex normal values
				else if (line.substr(0, 2) == "vn") {

					//do these only once
					if (readNorm == false) {
						createNormIndicesVar();
						readNorm = true;
					}
					std::stringstream ss(line);
					std::string temp;

					vector<float> v;
					float vx, vy, vz;

					//get float values 
					for (int i = 0;i<4;i++) {
						ss >> temp;
						if (i == 0) {//header
							continue;
						}
						else if (i == 1) {
							vx = std::stof(temp, 0);
						}
						else if (i == 2) {
							vy = std::stof(temp, 0);
						}
						else if (i == 3) {
							vz = std::stof(temp, 0);
						}
					}

					//place in container
					v.push_back(vx);
					v.push_back(vy);
					v.push_back(vz);
					normals.push_back(v);

					
				}
				
				//face values
				else if (line.substr(0, 2) == "f ") {
					std::string delim = "/";
					std::istringstream is(line);
					std::string triplet;
					int num = 0;
								
					vector<int> vert;
					int v1, v2, v3, v4;
					int n1, n2, n3, n4;
					while (is >> triplet) { //till end of line
						
						if (num == 0) {//header
							num++;
							continue;
						}
						
						//iterate through the three values separated by '/'
						std::string token;
						
						//which value in the triplet
						int val = 0;
						//current position
						int pos = 0;
						while ((pos = triplet.find(delim)) != std::string::npos) {
							//get the values
							token = triplet.substr(0, pos);
							bool tripletEnd = false;
							//get the vertex and convert to int
							if (val == 0 && num == 1) {
								v1 = std::stoi(token, 0)-1;
							}else if (val == 0 && num == 2) {
								v2 = std::stoi(token, 0)-1;
							}else if (val == 0 && num == 3) {
								v3 = std::stoi(token, 0)-1;
							}else if (val == 0 && num == 4) {
								v4 = std::stoi(token, 0)-1;
							}

							//remove from string
							triplet.erase(0, pos + delim.length());
							val++;
							if (val == 2) { //at the end of triplet
								token = triplet.substr(0, pos);
							}

							//get the vertex normals index and convert to int
							if (readNorm) {
								if (val == 2 && num == 1) {
									n1 = std::stoi(token, 0)-1;
								}
								else if (val == 2 && num == 2) {
									n2 = std::stoi(token, 0)-1;
								}
								else if (val == 2 && num == 3) {
									n3 = std::stoi(token, 0)-1;
								}
								else if (val == 2 && num == 4) {
									n4 = std::stoi(token, 0)-1;
								}
							}

							if (val == 2) {
								//increase the value down here
								num++;
							}
						}
					}
					//add vertices to face list
					vert.push_back(v1);
					vert.push_back(v2);
					vert.push_back(v3);
					if (num == 5) {
						vert.push_back(v4);
					}
					faces.push_back(vert);

					//add corresponding normals to an indices list to retrieve later
					if (readNorm) {
						if (normalIndices[v1] == 0) {
							normalIndices.insert(normalIndices.begin() + v1,n1);
						}
						if (normalIndices[v2] == 0) {
							normalIndices.insert(normalIndices.begin() + v2, n2);
						}
						if (normalIndices[v3] == 0) {
							normalIndices.insert(normalIndices.begin() + v3, n3);
						}
						if (num == 5) {
							if (normalIndices[v4] == 0) {
								normalIndices.insert(normalIndices.begin() + v4, n4);
							}
						}
					}
				}
				
				
				
				
			}
		}
		return true;
	}

	bool readFile() {
		//create file from path
		FILE * file = fopen(filepath.c_str(), "r");
		if (file == NULL) {
			printf("File failed to open !\n");
			return false;
		}
		else { //parse file



			while (true) {
				char header[150];

				int type = fscanf(file, "%s", header);
				if (type == EOF) {
					break;
				}
				else if (strcmp(header, "f") == 0) {
					//indices are at +1
					vector<int> v;
					std::string line;
					char temp;
					int v1, v2, v3, v4, unused;
					int vertexIndex[3];
					fscanf(file, "%d %d %d\n", &vertexIndex[0], &vertexIndex[1], &vertexIndex[2]);
					//unused = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &unused, &unused, &vertexIndex[1], &unused, &unused, &vertexIndex[2], &unused, &unused);
					
					v.push_back(vertexIndex[0] - 1);
					v.push_back(vertexIndex[1] - 1);
					v.push_back(vertexIndex[2] - 1);
					faces.push_back(v);

					/*fscanf(file, "%c", &temp);
					if (temp == '/n') {
						v.push_back(v1 - 1);
						v.push_back(v2 - 1);
						v.push_back(v3 - 1);
					}
					else {
						fscanf(file, "%d", &v4);
						v.push_back(v1 - 1);
						v.push_back(v2 - 1);
						v.push_back(v3 - 1);
						v.push_back(v4 - 1);
					}
					faces.push_back(v);*/

					/*bool done = false;
					char temp;
					int size = 0;
					int v1, v2, v3, v4;

					for (int i = 0; i < 4; i++) {
						scanf("%d%c", &v1, &temp);
						size++;
						if (temp == '\n') {
							break;
						}
						else if (size == 1) {
							scanf("%d%c", &v1, &temp);
							size++;
						}else if (i == 0) {
							scanf("%d%c", &v1, &temp);
							size++;
						}else if (i == 0) {
							scanf("%d%c", &v1, &temp);
							size++;
						}else if (i == 0) {
							scanf("%d%c", &v1, &temp);
							size++;
						}
						
					}
					
				

					char input;
					fscanf(file, "%s", &input);
					while (input != '/n') {

					}*/

				}
				else if (strcmp(header, "v") == 0) {
					vector<float> v;
					float vx, vy, vz;
					fscanf(file, "%f %f %f\n", &vx, &vy, &vz);
					v.push_back(vx);
					v.push_back(vy);
					v.push_back(vz);
					vertices.push_back(v);
				}
				else if (strcmp(header, "#") == 0) {
					//do nothing
				}
				else if (strcmp(header, "vn") == 0) {
					vector<float> v;
					float vx, vy, vz;
					fscanf(file, "%f %f %f\n", &vx, &vy, &vz);
					v.push_back(vx);
					v.push_back(vy);
					v.push_back(vz);
					normals.push_back(v);
					readNorm = true;
				}
			}
			
		}
		return true;
	}

};
#endif