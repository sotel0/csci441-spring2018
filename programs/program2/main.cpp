#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/matrix3.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>

#include "shape.h"
#include "model.h"
#include "camera.h"
#include "renderer.h"
#include "objparser.h"
#include "vectorOps.h"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;
int pressed = 0;
bool birdsEye = true;
ObjParser obj1 = ObjParser("Maze6.obj", 1, 1, 1);

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

Matrix processModel(Matrix& model, Camera& cam, GLFWwindow *window) {
    Matrix transMod;
	Vector transCam = { 0,0,0,0 };
	bool pressed = false;
	const float SPEED = 1000;
    const float TRANS = .0025;

	if (isPressed(window, GLFW_KEY_UP)) { 
		if (birdsEye) {
			transMod.translate(0, 0, TRANS);
			transCam.values[2] = TRANS;
		}
		else {
			Vector dir = (cam.origin - cam.eye).normalized();
			dir.values[0] = dir.values[0] / SPEED;
			dir.values[2] = dir.values[2] / SPEED;
			transMod.translate(dir.values[0], 0, dir.values[2]);
			transCam.values[0] = dir.values[0];
			transCam.values[2] = dir.values[2];
		}
		pressed = true;
	}
	else if (isPressed(window, GLFW_KEY_DOWN)) {
		if (birdsEye) {
			transMod.translate(0, 0, -TRANS);
			transCam.values[2] = -TRANS;
		}
		else {
			Vector dir = (cam.origin - cam.eye).normalized();
			dir.values[0] = -dir.values[0] / SPEED;
			dir.values[2] = -dir.values[2] / SPEED;
			transMod.translate(dir.values[0], 0, dir.values[2]);
			transCam.values[0] = dir.values[0];
			transCam.values[2] = dir.values[2];
		}
		pressed = true;

	}
	else if (isPressed(window, GLFW_KEY_LEFT)) { 
		if (birdsEye) {
			transMod.translate(TRANS, 0, 0);
			transCam.values[0] = TRANS;
		}
		else {
			Vector dir = (cam.origin - cam.eye).normalized();
			float temp = dir.values[0];
			dir.values[0] = dir.values[2] / SPEED;
			dir.values[2] = -temp / SPEED;

			transMod.translate(dir.values[0], 0, dir.values[2]);
			transCam.values[0] = dir.values[0];
			transCam.values[2] = dir.values[2];
			
		}
		pressed = true;
		
	}
	else if (isPressed(window, GLFW_KEY_RIGHT)) { 
		if (birdsEye) {
			transMod.translate(-TRANS, 0, 0);
			transCam.values[0] = -TRANS;
		}
		else {
			Vector dir = (cam.origin - cam.eye).normalized();
			float temp = dir.values[0];
			dir.values[0] = -dir.values[2] / SPEED;
			dir.values[2] = temp / SPEED;
			transMod.translate(dir.values[0], 0, dir.values[2]);
			transCam.values[0] = dir.values[0];
			transCam.values[2] = dir.values[2];
		}
		pressed = true;

	}
	if (pressed) {
		model = transMod * model;
		cam.eye = Vector(model.values[12], model.values[13], model.values[14]);
		cam.origin = cam.origin + transCam;
	}
    return model;
}

Vector processCam(Camera& cam, GLFWwindow *window) {
	Vector transf = { 0,0,0,0 };
	float TRANS = 0.0001f;
	float SPEED = 100.0f;
	float CLAMPZ = -0.94f;
	float CLAMPY1 = 2.03f;
	float CLAMPY2 = 1.97f;

	bool pressed = false;

	if (isPressed(window, GLFW_KEY_W)) {
		pressed = true;
		//std::cout << cam.origin.to_string() << " origin" << std::endl;
		//std::cout << cam.eye.to_string() << " eye" << std::endl;
		if (cam.origin.values[1] < 2.01f) {
			if (cam.origin.values[0] >= cam.eye.values[0]) {
				if (cam.origin.values[1] >= cam.eye.values[1]) {
					if (cam.origin.values[2] >= cam.eye.values[2]) {
						transf.values[0] = transf.values[0] - TRANS;
						transf.values[1] = transf.values[1] + TRANS;
						transf.values[2] = transf.values[2] - TRANS;
					}
					else {
						transf.values[0] = transf.values[0] - TRANS;
						transf.values[1] = transf.values[1] + TRANS;
						transf.values[2] = transf.values[2] + TRANS;
					}
				}
				else {
					if (cam.origin.values[2] >= cam.eye.values[2]) {
						transf.values[0] = transf.values[0] + TRANS;
						transf.values[1] = transf.values[1] + TRANS;
						transf.values[2] = transf.values[2] + TRANS;
					}
					else {
						transf.values[0] = transf.values[0] + TRANS;
						transf.values[1] = transf.values[1] + TRANS;
						transf.values[2] = transf.values[2] - TRANS;
					}
				}
			}
			else {
				if (cam.origin.values[1] >= cam.eye.values[1]) {
					if (cam.origin.values[2] >= cam.eye.values[2]) {
						transf.values[0] = transf.values[0] + TRANS;
						transf.values[1] = transf.values[1] + TRANS;
						transf.values[2] = transf.values[2] - TRANS;
					}
					else {
						transf.values[0] = transf.values[0] + TRANS;
						transf.values[1] = transf.values[1] + TRANS;
						transf.values[2] = transf.values[2] + TRANS;
					}
				}
				else {
					if (cam.origin.values[2] >= cam.eye.values[2]) {
						transf.values[0] = transf.values[0] - TRANS;
						transf.values[1] = transf.values[1] + TRANS;
						transf.values[2] = transf.values[2] + TRANS;
					}
					else {
						transf.values[0] = transf.values[0] - TRANS;
						transf.values[1] = transf.values[1] + TRANS;
						transf.values[2] = transf.values[2] - TRANS;
					}
				}
			}
		}
		
		
	} 
	else if (isPressed(window, GLFW_KEY_S)) {
		pressed = true;
		//std::cout << cam.origin.to_string() << " origin" << std::endl;
		//std::cout << cam.eye.to_string() << " eye" << std::endl;
		if (cam.origin.values[1] > 1.97f) {
			if (cam.origin.values[0] >= cam.eye.values[0]) {
				if (cam.origin.values[1] >= cam.eye.values[1]) {
					if (cam.origin.values[2] >= cam.eye.values[2]) {
						transf.values[0] = transf.values[0] + TRANS;
						transf.values[1] = transf.values[1] - TRANS;
						transf.values[2] = transf.values[2] + TRANS;
					}
					else {
						transf.values[0] = transf.values[0] + TRANS;
						transf.values[1] = transf.values[1] - TRANS;
						transf.values[2] = transf.values[2] - TRANS;
					}
				}
				else {
					if (cam.origin.values[2] >= cam.eye.values[2]) {
						transf.values[0] = transf.values[0] - TRANS;
						transf.values[1] = transf.values[1] - TRANS;
						transf.values[2] = transf.values[2] - TRANS;
					}
					else {
						transf.values[0] = transf.values[0] - TRANS;
						transf.values[1] = transf.values[1] - TRANS;
						transf.values[2] = transf.values[2] + TRANS;
					}
				}
			}
			else {
				if (cam.origin.values[1] >= cam.eye.values[1]) {
					if (cam.origin.values[2] >= cam.eye.values[2]) {
						transf.values[0] = transf.values[0] - TRANS;
						transf.values[1] = transf.values[1] - TRANS;
						transf.values[2] = transf.values[2] + TRANS;
					}
					else {
						transf.values[0] = transf.values[0] - TRANS;
						transf.values[1] = transf.values[1] - TRANS;
						transf.values[2] = transf.values[2] - TRANS;
					}
				}
				else {
					if (cam.origin.values[2] >= cam.eye.values[2]) {
						transf.values[0] = transf.values[0] + TRANS;
						transf.values[1] = transf.values[1] - TRANS;
						transf.values[2] = transf.values[2] - TRANS;
					}
					else {
						transf.values[0] = transf.values[0] + TRANS;
						transf.values[1] = transf.values[1] - TRANS;
						transf.values[2] = transf.values[2] + TRANS;
					}
				}
			}
		}
	}
	else if (isPressed(window, GLFW_KEY_A)) {
		pressed = true;
		if (cam.origin.values[0] >= cam.eye.values[0]) {

			transf.values[2] = transf.values[2] - TRANS;
			if (cam.origin.values[2] >= cam.eye.values[2]) {
				transf.values[0] = transf.values[0] + TRANS;
			}
			else {
				transf.values[0] = transf.values[0] - TRANS;

			}
		}
		else {

			transf.values[2] = transf.values[2] + TRANS;
			if (cam.origin.values[2] >= cam.eye.values[2]) {
				transf.values[0] = transf.values[0] + TRANS;
			}
			else {
				transf.values[0] = transf.values[0] - TRANS;

			}
		}

	}
	else if (isPressed(window, GLFW_KEY_D)) {
		pressed = true;
		if (cam.origin.values[0] >= cam.eye.values[0]) {

			transf.values[2] = transf.values[2] + TRANS;
			if (cam.origin.values[2] >= cam.eye.values[2]) {
				transf.values[0] = transf.values[0] - TRANS;
			}
			else {
				transf.values[0] = transf.values[0] + TRANS;

			}
		}
		else {

			transf.values[2] = transf.values[2] - TRANS;
			if (cam.origin.values[2] >= cam.eye.values[2]) {
				transf.values[0] = transf.values[0] - TRANS;
			}
			else {
				transf.values[0] = transf.values[0] + TRANS;

			}
		}
	}

	if (pressed) {
		cam.origin = cam.origin + transf;
	}

	return cam.origin;
}

void processInput(Model& obj, Camera& cam, GLFWwindow *window) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }

	if (isPressed(window, GLFW_KEY_SPACE)) {
		pressed = 1;
	}
	else if (isReleased(window, GLFW_KEY_SPACE)) {
		if (pressed == 1) {
			if (birdsEye) {
				birdsEye = false;
			}
			else {
				birdsEye = true;
			}
			pressed = 0;
		}
	}
	processCam(cam, window);
    obj.model = processModel(obj.model, cam, window);

}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "CSCI441-lab", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }
	//create the player model
	Model player(
			Sphere(40,.25,1,.2,.4).coords,
			Shader("../vert.glsl", "../frag.glsl"));
	Matrix obj2_trans, obj2_scale;
	obj2_trans.translate(-2, 2, -1);
	obj2_scale.scale(1, 1, 1);
	player.model = obj2_trans * obj2_scale;

	//creat the goal model
	Model goal(
		Sphere(40, .25, .2, 1, .4).coords,
		Shader("../vert.glsl", "../frag.glsl"));
	Matrix obj3_trans, obj3_scale;
	obj3_trans.translate(0, 2, 0);
	obj3_scale.scale(1, 1, 1);
	goal.model = obj3_trans * obj3_scale;

	//create the maze object
	Model maze(obj1.coords, Shader("../vert.glsl", "../frag.glsl"));
	//place object within view
	Matrix obj1_trans, obj1_scale, obj1_rotate;
	obj1_trans.translate(-3.5, 1.5, -3.5);
	obj1_scale.scale(1, 1, 1);
	obj1_rotate.rotate_y(180);
	maze.model = obj1_rotate*obj1_trans*obj1_scale;


    // make a floor
    Model floor(
            DiscoCube().coords,
            Shader("../vert.glsl", "../frag.glsl"));
    Matrix floor_trans, floor_scale;
    floor_trans.translate(0, 1, 0);
    floor_scale.scale(10, .5, 10);
    floor.model = floor_trans*floor_scale;

    // setup cameras
	
	//for 1st person camera
	Matrix projection1;
    projection1.perspective(45, 1, .31, 100);

	//for birds eye camera
    Matrix projection2;
	projection2.ortho(-5,5,-5,5,-100, 100);

	//1st person camera
	Camera camera1;
	camera1.projection = projection1;
	camera1.eye = Vector(player.model.values[12], player.model.values[13], player.model.values[14]);
	camera1.origin = Vector(player.model.values[12], player.model.values[13], player.model.values[14] + .1);
	camera1.up = Vector(0, 1, 0);

	//birds eye camera
	Camera camera2;
	camera2.projection = projection2;
	camera2.eye = Vector(-.3, 2, -1);
	camera2.origin = Vector(0, 0, 0);
	camera2.up = Vector(0, 1, 0);

	

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    // create a renderer
    Renderer renderer;

    // set the light position
    Vector lightPos(5.0f, 5.0f, 0.0f);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
		
		processInput(player, camera1, window);
		
        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // render the objects

		//only render player model in birds eye view, with orthographic projection
		if (birdsEye) {
			renderer.render(camera2, goal, lightPos);
			renderer.render(camera2, maze, lightPos);
			renderer.render(camera2, floor, lightPos);
			renderer.render(camera2, player, lightPos);
		}
		else {
			renderer.render(camera1, goal, lightPos);
			renderer.render(camera1, maze, lightPos);
			renderer.render(camera1, floor, lightPos);
			renderer.render(camera1, player, lightPos);

		}
		
        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
