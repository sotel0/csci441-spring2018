#ifndef _CSCI441_VECOPS_H_
#define _CSCI441_VECOPS_H_

#include <cmath>
#include <vector>

using std::vector;

//printing a matrix
template<typename T>
std::ostream& operator<< (std::ostream &os, const vector<vector<T>> m)
{
	for (int i = 0; i < m.size(); i++) {
		for (int j = 0; j < m[i].size(); j++) {
			os << " " << m[i][j] << " ";
		}
		os << std::endl;
	}
	return os;
}

//normalizing a vector
template<typename T>
vector<T> normalizeV(vector<T> v) {
	float mag = pow(pow(v[0], 2) + pow(v[1], 2) + pow(v[2], 2), 0.5);

	if (mag == 0) {
		std::cout << "normalize fail" << 0 << std::endl;
		return v;
	}
	vector<T> norm = { v[0] / mag, v[1] / mag, v[2] / mag };
	return norm;
}

//cross product between vectors
template<typename T>
vector<T> crossV(vector<T> a, vector<T> b) {
	if (b.size() != 3) {
		std::cout << "cross product fail" << std::endl;
		return a;
	}

	vector<float> cross =
	{ a[1] * b[2] - a[2] * b[1],
		a[2] * b[0] - a[0] * b[2],
		a[0] * b[1] - a[1] * b[0] };
	return cross;
}

//printing vectors
template<typename T>
std::ostream& operator<< (std::ostream &os, const vector<T> v)
{
	for (int i = 0; i < v.size(); i++) {
		os << " " << v[i] << " ";
	}
	os << std::endl;
	return os;
}
#endif