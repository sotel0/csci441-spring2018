Isaac Sotelo
April 05 2018
Program 2
==============
This program allows the user to control a ball and move it around a difficult "maze".

Press SPACE to swap between 1st person view and bird's eye view.
Defaults to 1st person..

In Bird's Eye View
	- move the ball with the corresponding ARROW KEY.

In 1st person
	- Move the direction you want with the correspond ARROW KEY.

	- Press 'A' to turn your view to the left.
	- Press 'D' to turn your view to the right.

	// The vertical values are *clamped*
	- Press 'W' to turn your view up.
	- Press 'S' to turn your view down.