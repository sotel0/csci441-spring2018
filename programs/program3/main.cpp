/*Isaac Sotelo
Program 3 Ray Tracing Triangles
*/

#include <iostream>

#include <glm/glm.hpp>

#include "bitmap_image.hpp"

enum ViewType { orthographic, perspective };
const int imageWidth = 640;
const int imageHeight = 480;
ViewType vt = orthographic;

class Shape {};

// STRUCTURES 
struct Viewport {
    glm::vec2 min;
    glm::vec2 max;

    Viewport(const glm::vec2& min, const glm::vec2& max)
        : min(min), max(max) {}
};

struct Light {
	glm::vec3 position;
	glm::vec3 color;

	Light(const glm::vec3& position, const glm::vec3& color)
		:position(position), color(color){
	}
};

struct Ray {
	glm::vec3 origin;
	glm::vec3 direction;

	Ray(const glm::vec3& origin, const glm::vec3& direction)
		: origin(origin), direction(glm::normalize(direction)) {
	}
};

struct Sphere : public Shape{
    int id;
    glm::vec3 center;
    glm::vec3 color;
    float radius;

    Sphere(const glm::vec3& center=glm::vec3(0,0,0),
            float radius=0,
            const glm::vec3& color=glm::vec3(0,0,0))
        : center(center), radius(radius), color(color) {
            static int id_seed = 0;
            id = ++id_seed;
        }
};

struct Triangle : public Shape{
	int id;
	glm::vec3 point1;
	glm::vec3 point2;
	glm::vec3 point3;
	glm::vec3 color;
	glm::vec3 norm;
	bool reflective;

	Triangle(const glm::vec3& point1 = glm::vec3(-1, -1, 0),
		const glm::vec3& point2 = glm::vec3(1, -1, 0),
		const glm::vec3& point3 = glm::vec3(0, -1, 0),
		const glm::vec3& color = glm::vec3(0, 0, 0),
		const bool& reflective = false)
		: point1(point1), point2(point2), point3(point3),
		color(color), reflective(reflective){

		//calculate the triangle normal
		glm::vec3 u = point2 - point1;
		glm::vec3 v = point3 - point1;
		norm = glm::normalize(glm::cross(u, v));

		static int id_seed = 0;
		id = ++id_seed;
	}
};

// PROTOTYPES
glm::vec3 castRay(Ray ray, const std::vector<Triangle>& world, const Light& light, const int depth = 0);
glm::vec3 phongShading(glm::vec3 triangleNorm, Light light, glm::vec3 color, glm::vec3 eye, glm::vec3 intersection);

glm::vec3 normalizeV3(const glm::vec3& vector) {
	float mag = pow(pow(vector[0], 2) + pow(vector[1], 2) + pow(vector[2], 2), 0.5);
	if (mag == 0) {
		return vector;
	}
	glm::vec3 norm = vector / mag;
	return norm;
}

float sign(float f) {
	if (f > 0) {
		return 1.0f;
	}
	else if (f < 0) {
		return -1.0f;
	}
	else {
		return 0.0f;
	}
}

bool checkSphereIntersect(Ray ray, Sphere sphere) {
	bool intersected = false;

	//solve the quadratic equation to see if sphere and ray intersect
	//A x^2 + B x + C = 0
	float A = glm::dot(ray.direction, ray.direction);
	float B = 2 * glm::dot(ray.direction, ray.origin - sphere.center);
	float C = glm::dot(ray.origin - sphere.center, ray.origin - sphere.center) - (sphere.radius*sphere.radius);
	
	float D = pow(B, 2) - 4*A*C;
	
	float Q = -1 * (B + sign(B) * sqrt(D));

	float x1 = Q / (2 * A);
	float x2 = (2 * C) / Q;
	
	if (D >= 0) {
		intersected = true;
		return intersected;
	}
	return intersected;
}

glm::vec4 checkTriangleIntersect(Ray ray, Triangle triangle) {
	//entry four set to 0 means no intersection
	glm::vec4 intersected(0,0,0,0);

	//use cramers rule
	glm::mat3 D = glm::mat3(
		triangle.point2.x - triangle.point1.x, triangle.point2.y - triangle.point1.y, triangle.point2.z - triangle.point1.z,
		triangle.point3.x - triangle.point1.x, triangle.point3.y - triangle.point1.y, triangle.point3.z - triangle.point1.z,
		-ray.direction.x, -ray.direction.y, -ray.direction.z);

	glm::mat3 Du = glm::mat3(
		ray.origin.x - triangle.point1.x, ray.origin.y - triangle.point1.y, ray.origin.z - triangle.point1.z,
		triangle.point3.x - triangle.point1.x, triangle.point3.y - triangle.point1.y, triangle.point3.z - triangle.point1.z,
		-ray.direction.x, -ray.direction.y, -ray.direction.z);

	glm::mat3 Dv = glm::mat3(
		triangle.point2.x - triangle.point1.x, triangle.point2.y - triangle.point1.y, triangle.point2.z - triangle.point1.z,
		ray.origin.x - triangle.point1.x, ray.origin.y - triangle.point1.y, ray.origin.z - triangle.point1.z,
		-ray.direction.x, -ray.direction.y, -ray.direction.z);

	glm::mat3 Dt = glm::mat3(
		triangle.point2.x - triangle.point1.x, triangle.point2.y - triangle.point1.y, triangle.point2.z - triangle.point1.z,
		triangle.point3.x - triangle.point1.x, triangle.point3.y - triangle.point1.y, triangle.point3.z - triangle.point1.z,
		ray.origin.x - triangle.point1.x, ray.origin.y - triangle.point1.y, ray.origin.z - triangle.point1.z );

	float detD = glm::determinant(D);
	float detDu = glm::determinant(Du);
	float detDv = glm::determinant(Dv);
	float detDt = glm::determinant(Dt);

	float u = detDu / detD;
	float v = detDv / detD;
	float t = detDt / detD;

	//check if ray intersects triangle
	if (t < 0) {
	}
	else if (v < 0 || v > 1) {
	}
	else if (u < 0 || 1 - v < u) {
	}
	else{
		//last entry set to 1 means there was an intersection
		intersected = glm::vec4(u, v, t, 1);
	}
	return intersected;
}

void renderS(bitmap_image& image, const std::vector<Sphere>& world) {
    // TODO: implement ray tracer

	Viewport view = Viewport(glm::vec2(-5,5), glm::vec2(5, -5));
	rgb_t color;
	float camDistance = 5;
	glm::vec3 camPos = glm::vec3(0, 0, -camDistance);
	

	for (int i = 0; i < imageWidth; i++) {
		for (int j = 0; j < imageHeight; j++) {

			//create the origin and direction vectors
			glm::vec3 origin;
			glm::vec3 direction;

			float ui = view.min[0] + (view.max[0] - view.min[0])*(i + .5) / imageWidth;
			float vj = view.min[1] + (view.max[1] - view.min[1])*(j + .5) / imageHeight;

			if (vt == orthographic) {
				origin = glm::vec3(ui, vj, 0);
				direction = glm::vec3(0, 0, -1);
			}
			else if (vt == perspective) {
				origin = camPos;
				direction = glm::vec3(ui, vj, 0) - camPos;
			}

			//create a ray at this point
			Ray ray = Ray(origin, direction);
			

			//check if ray intersects sphere
			for (int k = 0; k < world.size(); k++) {
				bool intersected = checkSphereIntersect(ray, world[k]);
				if (intersected) {
					//if it does create new colour
					color = make_colour(world[k].color[0]*255, world[k].color[1]*255, world[k].color[2]*255);
					break;
				}
				else {
					//set the image background to this color
					color = make_colour(75, 15, 211);
				}
			}

			//set color to pixel
			image.set_pixel(i,j,color);
		}
	}

}

void renderT(bitmap_image& image,const std::vector<Triangle>& world,
	const Light& light) {
	// TODO: implement ray tracer

	Viewport view = Viewport(glm::vec2(-5, 5), glm::vec2(5, -5));
	rgb_t color;
	float camDistance = 5;
	glm::vec3 camPos = glm::vec3(0, 0, -camDistance);

	for (int i = 0; i < imageWidth; i++) {
		for (int j = 0; j < imageHeight; j++) {

			//create the origin and direction vectors
			glm::vec3 origin;
			glm::vec3 direction;

			float ui = view.min[0] + (view.max[0] - view.min[0])*(i + .5) / imageWidth;
			float vj = view.min[1] + (view.max[1] - view.min[1])*(j + .5) / imageHeight;

			//dependent on projection
			if (vt == orthographic) {
				origin = glm::vec3(ui, vj, 0);
				direction = glm::vec3(0, 0, -1);
			}
			else if (vt == perspective) {
				origin = camPos;
				direction = glm::vec3(ui, vj, 0) - camPos;
			}

			//create a ray at this point
			Ray ray = Ray(origin, direction);

			//cast the ray, return color
			glm::vec3 resultColor = castRay(ray, world, light, 0);

			//set the color
			color = make_colour(resultColor.x * 255, resultColor.y * 255, resultColor.z * 255);

			//set color to pixel
			image.set_pixel(i, j, color);
		}
	}

}

glm::vec3 castRay(Ray ray, const std::vector<Triangle>& world, const Light& light, int depth) {
	glm::vec3 backgroundColor(0.29f, 0.05f, 0.82f);
	glm::vec3 resultColor;
	if (depth > 2) {
		resultColor = backgroundColor;
	}
	else {
		for (int k = 0; k < world.size(); k++) {

			//check if ray intersected object
			glm::vec4 intersection = checkTriangleIntersect(ray, world[k]);
			if (intersection[3] == 1) {
				//point intersected
				glm::vec3 pointIntersected = ray.origin + intersection[2] * ray.direction;

				//if new surface is reflective, cast ray again until color reached
				if (world[k].reflective) {

					//create reflected ray
					glm::vec3 newDirection = normalize(reflect(ray.direction, world[k].norm));
					glm::vec3 newOrigin(pointIntersected + world[k].norm);
					Ray newRay(newOrigin, newDirection);

					resultColor += 0.8f*castRay(newRay, world, light, depth + 1);
				}
				else {
					//if it does create new colour
					glm::vec3 colored(world[k].color[0], world[k].color[1], world[k].color[2]);

					//add the lighting effects
					glm::vec3 lightColor = phongShading(world[k].norm, light, colored, ray.origin, pointIntersected);

					resultColor = lightColor;
				}
				break;
			}
			else {
				//set the image background to this color if no intersection
				resultColor = backgroundColor;
			}
		}
	}
	return resultColor;
}

glm::vec3 phongShading(glm::vec3 triangleNorm, Light light, glm::vec3 color, glm::vec3 eye, glm::vec3 intersection){
	glm::vec3 result;
	float ambientStrength = 0.6;
	float specularStrength = 0.5;
	
	//ambient shading
	glm::vec3 ambient = ambientStrength * light.color;

	//diffuse shading
	glm::vec3 lightDir = normalize(light.position - intersection);
	float diff = glm::max(glm::dot(triangleNorm, lightDir), 0.0f);
	glm::vec3 diffuse = diff * light.color;

	//specular shading
	glm::vec3 viewDir = normalize(eye - normalize(intersection));
	glm::vec3 reflectDir = normalize(glm::reflect(-lightDir, triangleNorm));
	float spec = pow(glm::max(glm::dot(viewDir, reflectDir), 0.0f), 24.0f);
	glm::vec3 specular = specularStrength * spec * light.color;
	
	result = (ambient + diffuse + specular)*color;

	return result;
}

int main(int argc, char** argv) {

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(imageWidth,imageHeight);

	//place lighting
	glm::vec3 lightPos(0.55f, -0.75f, 1.0f);
	glm::vec3 lightColor(1.0f,1.0f,1.0f);
	Light worldLight(lightPos,lightColor);

	//create objects in world
	std::vector<Triangle> world{
		//Triangle(glm::vec3(-0.75,-1.75,-1),glm::vec3(1.25,-1.75,-1),glm::vec3(0.25,0.75,-1),glm::vec3(1,1,0)), //yellow
		//Triangle(glm::vec3(-2.25,-1.75,-1),glm::vec3(-0.25,-1.75,-1),glm::vec3(-1.25,0.25,-1),glm::vec3(1,0,0)),//red
		Triangle(glm::vec3(-1.5,-0.25,-1),glm::vec3(0.5,-0.25,-1),glm::vec3(-0.5,1.75,-1),glm::vec3(1,0,1)), //pink
		Triangle(glm::vec3(-3.0,-2.5,-5),glm::vec3(-3.0,-5.0,-1),glm::vec3(6.0,-2.5,-5),glm::vec3(1.0f, 1.0f, 1.0f),true),
		//Triangle(glm::vec3(4.0,-2.5,-5),glm::vec3(4.0,-5.0,-1),glm::vec3(-4.0,-5.0,-1),glm::vec3(1.0f, 1.0f, 1.0f),true)

	};

    // render the world
	renderT(image, world, worldLight);

    image.save_image("../triangle-ray-traced.bmp");
    std::cout << "Success" << std::endl;
}


