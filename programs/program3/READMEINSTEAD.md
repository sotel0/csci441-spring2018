# Ray Traced Triangles

## Implemented Functions

A ray tracer was created to render triangles by checking where/if the ray
intersected the drawn triangles. Phong Shading was also implemented as the
main lighting effect. Reflections were attempted, but the reflective surface
appeared to just absorb all of the color from any reflected object. The program
can simply be run and will produce a rendered triangle with phong shading with
a larger reflective triangle below. 

There are also images of the process of the results of the program.
